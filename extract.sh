#!/bin/sh

INFILE=$1

if test -z $INFILE
then
        INFILE=/dev/zero
fi

dd if=$INFILE bs=1 skip=`echo "ibase=16;0000" | bc` count=`echo "ibase=16;0149-0000" | bc` >data/font.bin
dd if=$INFILE bs=1 skip=`echo "ibase=16;0D37" | bc` count=`echo "ibase=16;0E17-0D37" | bc` >data/security.bin
dd if=$INFILE bs=1 skip=`echo "ibase=16;1641" | bc` count=`echo "ibase=16;1717-1641" | bc` >data/tbl_F641.bin
dd if=$INFILE bs=1 skip=`echo "ibase=16;1736" | bc` count=`echo "ibase=16;1CA6-1736" | bc` >data/tbl_F736.bin
dd if=$INFILE bs=1 skip=`echo "ibase=16;1CA6" | bc` count=`echo "ibase=16;1E36-1CA6" | bc` >data/tbl_FCA6.bin
