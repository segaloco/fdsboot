.ifndef MEM_I
MEM_I = 1

.include	"system/cpu.i"
.include	"system/fds.i"

work		= RAM_BASE
work_00         = work+$00
work_01		= work+$01
work_02         = work+$02
work_03         = work+$03
work_04         = work+$04
work_05         = work+$05
work_06         = work+$06
work_07         = work+$07
work_08         = work+$08
work_09         = work+$09
work_0A         = work+$0A
work_0B         = work+$0B
work_0C         = work+$0C
fds_filetbl_load_idx  = work+$0E
; -----------------------------
work_tbl_40	= $40
work_tbl_40_end	= $58
; -----------------------------
work_tbl_80	= $80
work_tbl_80_end	= work_tbl_80+$20
work_tbl_A0	= work_tbl_80_end
work_tbl_A0_end	= work_tbl_A0+$20
work_tbl_C0	= work_tbl_A0_end
work_tbl_C0_end	= work_tbl_C0+$10
work_tbl_D0     = work_tbl_C0_end
work_tbl_D0_end = work_tbl_D0+$10
; -----------------------------
work_E1		= $E1

work_E3		= $E3
work_E4		= work_E3+1
work_E5		= work_E4+1
work_E6		= work_E5+1
work_E7		= work_E6+1
work_E8		= work_E7+1
work_E9		= work_E8+1
work_EA		= work_E9+1
work_EB		= work_EA+1
; -----------------------------
work_end		= FDS_JOYPAD_HOLD_2
work_tbl_F1_end		= FDS_JOYPAD_PRESS
work_tbl_F1		= work_tbl_F1_end-4
; ------------------------------------------------------------
oam_buffer		= $200
; ------------------------------------------------------------
ppu_displist		= $300
byte_300                = $300
byte_301                = $301
byte_302                = $302
byte_303                = $303
PPU_VRAM_TBL_AR_HI      = 0
PPU_VRAM_TBL_AR_LO      = 1
PPU_VRAM_TBL_AR_BYTE    = 2
PPU_VRAM_TBL_ENTRY_SIZE = 3
; ------------------------------------------------------------
FDSINT0		= $DFF6
FDSINT1		= $DFF8
FDSINT2		= $DFFA
FDSSTART	= $DFFC
FDSIRQ		= $DFFE

.endif	; MEM_I
