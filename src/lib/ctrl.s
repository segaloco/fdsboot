.include	"system/ppu.i"
.include	"system/ctrl.i"
.include	"system/fds.i"

.include	"tunables.i"
.include	"mem.i"

	.export	fds_ctrl_read
fds_ctrl_read:
        lda     FDS_CTRL0_B
        and     #%11111000
        sta     FDS_CTRL0_B
        ora     #%00000101
        sta     CTRL0

        nop
        nop
        nop
        nop
        nop
        nop

        ldx     #8
	: ; for (x = 8; x >= 0; x--) {
		lda     FDS_CTRL0_B
		ora     #%00000100
		sta     CTRL0

		ldy     #10
		: ; for (y = 10; y > 0; y--) {
			dey
			bne     :-
		; }
		nop

		ldy     FDS_CTRL0_B
		lda     CTRL1
		lsr     a
		and     #%00001111
		beq     :++

		sta     work_00, x
		lda     FDS_CTRL0_B
		ora     #%00000110
		sta     CTRL0

		ldy     #10
		: ; for (y = 10; y > 0; y--) {
			dey
			bne     :-
		; }
		nop
		nop

		lda     CTRL1
		rol     a
		rol     a
		rol     a
		and     #%11110000
		ora     work_00, x
		eor     #<~0
		sta     work_00, x

		dex
		bpl     :---
	; } if () {
		ldy     FDS_CTRL0_B
		ora     #<~0
	: ; }

	sty     CTRL0
        rts
