.include	"system/ppu.i"
.include	"system/fds.i"

	.export	fds_ppu_px_to_nt
fds_ppu_px_to_nt:
	lda	#>(PPU_VRAM_BG1>>2)
	sta	fdsbios_ppu_ntaddr
	lda	fdsbios_ppu_y_px
	asl	a
	rol	fdsbios_ppu_ntaddr
	asl	a
	rol	fdsbios_ppu_ntaddr
	and	#%11100000
	sta	fdsbios_ppu_ntaddr+1
	lda	fdsbios_ppu_x_px
	lsr	a
	lsr	a
	lsr	a
	ora	fdsbios_ppu_ntaddr+1
	sta	fdsbios_ppu_ntaddr+1
	rts

	.export	fds_ppu_nt_to_px
fds_ppu_nt_to_px:
	lda	fdsbios_ppu_ntaddr+1
	asl	a
	asl	a
	asl	a
	sta	fdsbios_ppu_x_px
	lda	fdsbios_ppu_ntaddr+1
	sta	fdsbios_ppu_y_px
	lda	fdsbios_ppu_ntaddr
	lsr	a
	ror	fdsbios_ppu_y_px
	lsr	a
	ror	fdsbios_ppu_y_px
	lda	#%11111000
	and	fdsbios_ppu_y_px
	sta	fdsbios_ppu_y_px
	rts
