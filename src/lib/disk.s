.include	"system/ppu.i"
.include	"system/ctrl.i"
.include	"system/fds.i"

.include	"tunables.i"
.include	"mem.i"

fds_disk_ptr_disk_id	= work_00
fds_filetbl_entry	= work_02
fds_filecount		= work_06
fds_file_max		= work_08
fds_rx_skip		= work_09
fds_disk_buffer		= work_0A
fds_disk_buffer_size	= work_0C
; -----------------------------------------------------------
	.export fds_filetbl_load
fds_filetbl_load:
	lda     #0
        sta     fds_filetbl_load_idx
        lda     #FDS_DISKSET_2ADDR
        jsr     fds_disk_ptr_nocheck

        lda     FDS_STACK_IRQMODE
        pha

        lda     #FDSBIOS_FILETBL_LOAD_TRY_COUNT
        sta     work_05
	: ; for (i = FDS_FILETBL_LOAD_TRIES; fds_filetbl_load_do() != 0 && i > 0; i--) {
		jsr     fds_filetbl_load_do
		beq     :+ ; if (fds_filetbl_load_do() != 0) {
			dec     work_05
			bne     :-
		; } else break;
	: ; }

	pla
        sta     FDS_STACK_IRQMODE

        ldy     fds_filetbl_load_idx
        txa
        rts
; ----------------------------
fds_filetbl_load_do:
	jsr     fds_diskhead_check
        jsr     fds_filecount_get

        lda     fds_filecount
        beq     :++
	: ; for (file of filetbl) {
		lda     #FDS_FILEHEADER_ID
		jsr     fds_blocktype_check
		jsr     fds_file_match
		jsr     fds_data_load

		dec     fds_filecount
		bne     :-
	: ; }

	jsr     fds_load_done
        rts
; -----------------------------------------------------------
	.export	fds_file_append
	.export	fds_file_write
fds_file_append:
        lda     #FDSBIOS_FILETBL_END
fds_file_write:
        sta     fds_filetbl_load_idx
        lda     #FDS_DISKSET_2ADDR
        jsr     fds_disk_ptr
        lda     FDS_STACK_IRQMODE
        pha

        lda     #3
        sta     work_05
	: ; for (i = 3; i > 0 && fds_file_write_last(); i--) {
		dec     work_05
		beq     fds_file_write_done

		jsr     fds_file_write_last
		bne     :-
	; }

        lda     #2
        sta     work_05
	: ; for (i = 2; fds_file_check_last() && i > 0; i--) {
		jsr     fds_file_check_last
		beq     fds_file_write_done

		dec     work_05
		bne     :-
	; }

        stx     work_05
        jsr     fds_filecount_set
        ldx     work_05

fds_file_write_done:
	pla
        sta     FDS_STACK_IRQMODE
        txa
        rts
; -----------------------------------------------------------
fds_file_write_last:
	jsr     fds_diskhead_check
        lda     fds_filetbl_load_idx
        cmp     #FDSBIOS_FILETBL_END
        bne     :++ ; if (load_idx == FDSBIOS_FILETBL_END) {
		jsr     fds_filecount_get
	: jsr     fds_seek_end
	lda     #FDS_FILEHEADER_ID
	jsr     fds_fileblk_write
	lda     #0
	jsr     fds_data_save
	jsr     fds_load_done
	rts
	: ; } else {
		sta     fds_filecount
		jsr     fds_filecount_set_do
		jmp     :--
	; }
; -----------------------------------------------------------
fds_file_check_last:
	jsr     fds_diskhead_check
        ldx     fds_filecount
        inx
        txa
        jsr     fds_filecount_set_do
        jsr     fds_seek_end
        lda     #FDS_FILEHEADER_ID
        jsr     fds_blocktype_check
        lda     #<~0
        jsr     fds_data_save
        jsr     fds_load_done
        rts
; -----------------------------------------------------------
fds_filecount_set:
	jsr     fds_diskhead_check
        lda     fds_filecount
        jsr     fds_filecount_set_do
        jsr     fds_load_done
        rts
; -----------------------------------------------------------
fds_filecount_put_addrsize	= work_09

fds_filecount_put:
        ldx     #FDS_DISKSET_2ADDR
        bne     :+

fds_filecount_update:	
	ldx     #FDS_DISKSET_1ADDR

	: stx     fds_filecount_put_addrsize

        jsr     fds_disk_ptr

        lda     FDS_STACK_IRQMODE
        pha

        lda     #3
        sta     work_05
	: ; for (i = 3; i > 0 && fds_filecount_get(); i--) {
		dec     work_05
		beq     :++++

		jsr     fds_filecount_check
		bne     :-
	; }
	; if () {
		lda     fds_filecount
		sec
		sbc     work_02
		ldx     fds_filecount_put_addrsize
		beq     :+ ; if (addrsize == 2) {
			lda     work_02
		: ; }

		ldx     #fds_errnos::update_count_fail
		bcc     :++ ; if () {
			sta     fds_filecount

			lda     #2
			sta     work_05
			: ; for (i = 2; fds_filecount_set() && i > 0; i--) {
				jsr     fds_filecount_set
				beq     :+

				dec     work_05
				bne     :-
			: ; }
		; }
	: ; }

	pla
        sta     FDS_STACK_IRQMODE

        txa
        rts
; -----------------------------------------------------------
fds_filecount_check:
	jsr     fds_diskhead_check
        jsr     fds_filecount_get
        jsr     fds_load_done
        rts
; -----------------------------------------------------------
fds_filecount_sync:
        ldx     #1
        bne     :+

fds_file_idx_sync:
	ldx     #0

	: stx	work_07
        jsr     fds_disk_ptr
        lda     FDS_STACK_IRQMODE
        pha
        clc
        lda     work_02
        adc     work_07
        sta     fds_filecount

        lda     #2
        sta     work_05
	: ; for (i = 2; fds_filecount_set() && i > 0; i--) {
		jsr     fds_filecount_set
		beq     :+

		dec     work_05
		bne     :-
	: ; }

	pla
        sta     FDS_STACK_IRQMODE
        txa
        rts
; -----------------------------------------------------------
fds_disk_stat:
        lda     #FDS_DISKSET_1ADDR
        jsr     fds_disk_ptr_nocheck
        lda     FDS_STACK_IRQMODE
        pha

        lda     #2
        sta     work_05
	: ; for (i = 2; fds_tx() && i > 0; i--) {
		jsr     fds_tx
		beq     :+

		dec     work_05
		bne     :-
	: ; }

	pla
        sta     FDS_STACK_IRQMODE
        txa
        rts
; -----------------------------------------------------------
fds_tx_sum	= work_02

fds_tx:
	jsr     fds_header_check

        lda     fds_disk_ptr_disk_id
        sta     fds_disk_buffer
        lda     fds_disk_ptr_disk_id+1
        sta     fds_disk_buffer+1

        ldy     #0
        sty     fds_tx_sum
        sty     fds_tx_sum+1
	: ; for (byte of diskheader_id_block) {
		jsr     fds_irq_transfer
		sta     (fds_disk_buffer), y

		iny
		cpy     #FDS_DISKHEADER_CHECK_END-FDS_DISKHEADER_STR_LAST
		bne     :-
	; }
        jsr     fds_tx_inc

        ldy     #FDS_DISKHEADER_SIZE-FDS_DISKHEADER_CHECK_END
	: ; for (remaining_bytes of diskheader) {
		jsr     fds_irq_transfer

		dey
		bne     :-
	; }

        jsr     fds_blk_crc
        jsr     fds_filecount_get
        ldy     #0
        lda     fds_filecount
        sta     (fds_disk_buffer), y
        beq     :+++
	: ; for (i = fds_filecount_get(); i > 0; i--) {
		lda     #FDS_FILEHEADER_ID
		jsr     fds_blocktype_check
		jsr     fds_irq_transfer
		jsr     fds_irq_transfer

		ldy     #1
		sta     (fds_disk_buffer), y
		: ; for (byte of name) { 
			iny

			jsr     fds_irq_transfer
			sta     (fds_disk_buffer), y

			cpy     #FDS_FILEHEADER_NAME_SIZE+1
			bne     :-
		; }
		jsr     fds_tx_inc

		jsr     fds_irq_transfer
		jsr     fds_irq_transfer
		clc
		lda     #%00000101
		adc     fds_tx_sum
		sta     fds_tx_sum
		lda     #%00000001
		adc     fds_tx_sum+1
		sta     fds_tx_sum+1
		jsr     fds_irq_transfer
		sta     fds_disk_buffer_size
		jsr     fds_irq_transfer
		sta     fds_disk_buffer_size+1
		clc
		lda     fds_disk_buffer_size
		adc     fds_tx_sum
		sta     fds_tx_sum
		lda     fds_disk_buffer_size+1
		adc     fds_tx_sum+1
		sta     fds_tx_sum+1

		lda     #<~0
		sta     fds_rx_skip
		jsr     fds_rx

		dec     fds_filecount
		bne     :--
	: ; }

	lda     fds_tx_sum+1
        ldy     #1
        sta     (fds_disk_buffer), y
        lda     fds_tx_sum
        iny
        sta     (fds_disk_buffer), y
        jsr     fds_load_done
        rts
; ----------------------------
fds_tx_inc:
	tya
        clc
        adc     fds_disk_buffer
        sta     fds_disk_buffer
        lda     #0
        adc     fds_disk_buffer+1
        sta     fds_disk_buffer+1
        rts
; -----------------------------------------------------------
fds_disk_ptr_addr_count	= work_02
fds_disk_ptr_stack_next	= work_04
fds_disk_ptr_jsr_loc	= work_05

fds_disk_ptr_nocheck:
	sec
        bcs     :+

fds_disk_ptr:
	clc

	: tsx
        dex
        stx     fds_disk_ptr_stack_next
        php

        sta     fds_disk_ptr_addr_count
        ldy     FDS_STACK_WORKING, x
        sty     fds_disk_ptr_jsr_loc
        ldy     FDS_STACK_WORKING+1, x
        sty     fds_disk_ptr_jsr_loc+1
        tax
        ldy     #1
        lda     (fds_disk_ptr_jsr_loc), y
        sta     fds_disk_ptr_disk_id
        iny
        lda     (fds_disk_ptr_jsr_loc), y
        sta     fds_disk_ptr_disk_id+1
        lda     #WORD_SIZE
        cpx     #FDS_DISKSET_2ADDR
        bne     :+ ; if (addr_count == FDS_DISKSET_2ADDR) {
		iny
		lda     (fds_disk_ptr_jsr_loc), y
		sta     fds_filetbl_entry
		iny
		lda     (fds_disk_ptr_jsr_loc), y
		sta     fds_filetbl_entry+1
		lda     #WORD_SIZE*2
	: ; }
	ldx     fds_disk_ptr_stack_next
        clc
        adc     fds_disk_ptr_jsr_loc
	sta	FDS_STACK_WORKING, x
	lda	#0
        adc     fds_disk_ptr_jsr_loc+1
        sta     FDS_STACK_WORKING+1, x

        plp
        ldx     #fds_errnos::disk_set
        lda     FDS_DISK_SR
        and     #(fds_disk_sr::drive_empty)
        bne     :+
        bcs     :++
        ldx     #fds_errnos::read_only
        lda     FDS_DISK_SR
        and     #(fds_disk_sr::drive_ro)
        beq     :++
	: ; if (fds_drive_empty || fds_drive_ro) {
		pla
		pla
		ldy     fds_filetbl_load_idx
		txa
		cli
	: ; }

	rts
; -----------------------------------------------------------
fds_diskhead_check_errno	= work_08

fds_diskhead_check:
	jsr     fds_header_check

        ldx     #fds_errnos::headererrs
        stx     fds_diskhead_check_errno
        ldy     #0
	: ; for (field of header) {
		jsr     fds_irq_transfer
		cmp     (fds_disk_ptr_disk_id), y
		beq     :++ ; if (fds_irq_transfer() == addr1[y]) {
			ldx     fds_diskhead_check_errno
			cpx     #(FDS_DISKHEADER_UNK_IDX-FDS_DISKHEADER_STR_LAST)
			bne     :+ ; if (field == UNK) {
				ldx     #fds_errnos::errno_10
			: ; }

			lda     (fds_disk_ptr_disk_id), y
			cmp     #FDSBIOS_FILETBL_END
			jsr     fds_tx_fail
		: ; }
		iny
		cpy     #(FDS_DISKHEADER_NAME_IDX-FDS_DISKHEADER_STR_END_IDX)
		beq     :+
		cpy     #(FDS_DISKHEADER_DISK_ID_IDX-FDS_DISKHEADER_STR_END_IDX)
		bcc     :++
		: ; if (field == name || field >= disk_id) {
			inc     fds_diskhead_check_errno
		: ; }

		cpy     #(FDS_DISKHEADER_CHECK_END-FDS_DISKHEADER_STR_LAST)
		bne     :-----
	; }

        jsr     fds_irq_transfer
        sta     fds_file_max

        ldy     #(FDS_DISKHEADER_SIZE-FDS_DISKHEADER_DATA_END)
	: ; for (remaining_bytes of diskheader) {
		jsr     fds_irq_transfer

		dey
		bne     :-
	; }

        jsr     fds_blk_crc
        rts
; -----------------------------------------------------------
fds_filecount_get:
	lda     #FDS_FILECOUNT_ID
        jsr     fds_blocktype_check

        jsr     fds_irq_transfer
        sta     fds_filecount

        jsr     fds_blk_crc
        rts
; -----------------------------------------------------------
fds_filecount_set_do:
	pha

        lda     #FDS_FILECOUNT_ID
        jsr     fds_fileblk_write

        pla
        jsr     fds_irq_transfer
        jsr     fds_tx_end
        rts
; -----------------------------------------------------------
fds_file_match:
	jsr     fds_irq_transfer
        jsr     fds_irq_transfer
        lda     #fds_irqmode::skip_bytes|FDS_FILEHEADER_NAME_SIZE
        sta     FDS_STACK_IRQMODE
        cli

        ldy     #0
        lda     (fds_filetbl_entry), y
        cmp     #FDSBIOS_FILETBL_END
        beq     :+++
	: ; for (y = 0; y < ENTRY_MAX && filetbl_entry.not_in(0, FILETBL_END); y++) {
		txa
		cmp     (fds_filetbl_entry), y
		beq     :+++

		iny
		cpy     #FDSBIOS_FILETBL_ENTRY_MAX
		beq     :+
		lda     (fds_filetbl_entry), y
		cmp     #FDSBIOS_FILETBL_END
		bne     :-
	: ; }

	; if (!filetbl.empty && x != filetbl_entry) {
		lda     #<~0
		bne     :+++
	: ; }
	
	cpx     fds_file_max
        beq     :+
        bcs     :++
	: ; if (x == filetbl_entry || x <= fds_file_max) {
		lda     #0
		inc     fds_filetbl_load_idx
	: ; }
	sta     fds_rx_skip

	: ; while (FDS_STACK_IRQMODE) {
		lda     FDS_STACK_IRQMODE
		bne     :-
	; }

        rts
; -----------------------------------------------------------
fds_seek_end:
	lda     fds_filecount
        sta     work_08
        beq     :+++
	: ; for (file of filecount) {
		lda     #FDS_FILEHEADER_ID
		jsr     fds_blocktype_check
		ldy     #FDS_FILEHEADER_ADDR_IDX-FDS_FILEHEADER_FILENO_IDX
		: ; for (byte of header_info) {
			jsr     fds_irq_transfer

			dey
			bne     :-
		; ; }

		lda     #<~0
		sta     fds_rx_skip
		jsr     fds_data_load

		dec     work_08
		bne     :--
	: ; }

	rts
; -----------------------------------------------------------
fds_data_load:
	ldy     #0
	: ; for (y = 0; y < 4; y++) {
		jsr     fds_irq_transfer_s
		sta     fds_disk_buffer, y

		iny
		cpy     #4
		bne     :-
	; }
; ----------------------------
fds_rx:
	jsr     fds_buffer_size_dec
        jsr     fds_irq_transfer
        pha

        jsr     fds_blk_crc

        lda     #FDS_FILEBLOB_ID
        jsr     fds_blocktype_check
        ldy     fds_rx_skip

        pla
        bne     :++++ ; if (block_type == PRG) {
		clc
		lda     fds_disk_buffer
		adc     fds_disk_buffer_size
		lda     fds_disk_buffer+1
		adc     fds_disk_buffer_size+1
		bcs     :+
		lda     fds_disk_buffer+1
		cmp     #>PPU_CTLR0
		bcs     :++
		and     #>RAM_MASK
		cmp     #>oam_buffer
		bcs     :++
		: ; if (fds_disk_buffer_carry || (fds_disk_buffer < ppu_range && (fds_disk_buffer & RAM_MASK) < oam_buffer)) {
			ldy     #<~0
		: ; }
		; for (y; fds_buffer_size_dec(); y++) {
			jsr     fds_irq_transfer
			cpy     #0
			bne     :+ ; if (!fds_rx_skip) {
				sta     (fds_disk_buffer), y
				inc     fds_disk_buffer
				bne     :+
				inc     fds_disk_buffer+1
			: ; }

			jsr     fds_buffer_size_dec
			bcs     :--
			bcc     :++++
		; }
	: ; } else {
		cpy     #0
		bne     :+ ; if (!fds_rx_skip) {
			lda     FDS_PPU_CTLR1_B
			and     #<~(ppu_ctlr1::objblk_off|ppu_ctlr1::bgblk_off)
			sta     FDS_PPU_CTLR1_B
			sta     PPU_CTLR1
			lda     PPU_SR
			lda     fds_disk_buffer+1
			sta     PPU_VRAM_AR
			lda     fds_disk_buffer
			sta     PPU_VRAM_AR
		; }
		: ; while (fds_buffer_size_dec()) {
			jsr     fds_irq_transfer
			cpy     #0
			bne     :+ ; if (!fds_rx_skip) {
				sta     PPU_VRAM_IO
			: ; }

			jsr     fds_buffer_size_dec
			bcs     :--
		; }
	: ; }

	lda     fds_rx_skip
        bne     :+ ; if (!fds_rx_skip) {
		jsr     fds_blk_crc
		rts
	: ; } else {
		jsr     fds_irq_transfer
		jsr     fds_irq_transfer
		jmp     fds_tx_check
	; }
; -----------------------------------------------------------
fds_buffer_desc_load:
	ldy     #$0B
        lda     (work_02), y
        sta     fds_disk_buffer_size
        iny
        lda     (work_02), y
        sta     fds_disk_buffer_size+1
        ldy     #$0E
        lda     (work_02), y
        sta     fds_disk_buffer
        iny
        lda     (work_02), y
        sta     fds_disk_buffer+1
        iny
        lda     (work_02), y
        beq     :+ ; if () {
		jsr     fds_ppuctlr1_blk_on
		lda     PPU_SR
		lda     fds_disk_buffer+1
		sta     PPU_VRAM_AR
		lda     fds_disk_buffer
		sta     PPU_VRAM_AR
		lda     PPU_VRAM_IO
	: ; }

	jsr     fds_buffer_size_dec
        rts
; -----------------------------------------------------------
fds_data_save:
	sta     fds_rx_skip
        lda     fds_filecount
        jsr     fds_irq_transfer
        ldx     fds_rx_skip
        beq     :+ ; if (rx_skip) {
		ldx     #fds_errnos::savefail
		cmp     fds_filecount
		jsr     fds_tx_fail
	: ; }

	ldy     #0
	: ; for (y = 0; y < 0x0E; y++) {
		lda     (work_02), y
		jsr     fds_irq_transfer
		ldx     fds_rx_skip
		beq     :+ ; if (rx_skip) {
			ldx     #fds_errnos::savefail
			cmp     (work_02), y
			jsr     fds_tx_fail
		: ; }

		iny
		cpy     #$0E
		bne     :--
	; }

        ldx     fds_rx_skip
        beq     :++++
        jsr     fds_blk_crc
        jsr     fds_buffer_desc_load
        lda     #FDS_FILEBLOB_ID
        jsr     fds_blocktype_check
	: ; while (work[2+0x10] == 0) {
		ldy     #$10
		lda     (work_02), y
		bne     :++++

		ldy     #0
		ldx     fds_rx_skip
		beq     :++
		: ; if (rx_skip) while (fds_buffer_inc()) {
			jsr     fds_irq_transfer
			ldx     #fds_errnos::savefail
			cmp     (fds_disk_buffer), y
			jsr     fds_tx_fail

			jsr     fds_buffer_inc
			bcs     :-
			bcc     fds_data_save_end
		: ; } else while (fds_buffer_inc()) {
			lda     (fds_disk_buffer), y
			jsr     fds_irq_transfer

			jsr     fds_buffer_inc
			bcs     :-
			bcc     fds_data_save_end
		: ; }

		jsr     fds_tx_end
		jsr     fds_buffer_desc_load
		lda     #FDS_FILEBLOB_ID
		jsr     fds_fileblk_write
		jmp     :----
	: ; }

	: ; for (fds_buffer_desc_load(); fds_buffer_size_dec(); buffer--) {
		ldx     fds_rx_skip
		beq     :++ ; if (rx_skip) {
			jsr     fds_irq_transfer
			ldx     #fds_errnos::savefail
			cmp     PPU_VRAM_IO
			jsr     fds_tx_fail
		: ; }

		jsr     fds_buffer_size_dec
		bcs     :--

	fds_data_save_end:
		ldx     fds_rx_skip
		beq     fds_data_save_tx_end
		jsr     fds_blk_crc
		rts

		: ; if (!rx_skip) {
			lda     PPU_VRAM_IO
			jsr     fds_irq_transfer
			jmp     :--
		; }

	fds_data_save_tx_end:
		jsr     fds_tx_end
		rts
	; }
; -----------------------------------------------------------
fds_drive_wait:
	jsr     fds_io_mode_r_init

        ldy     #FDS_DRIVE_WAIT_MS
        jsr     fds_sleep_ms

        lda     FDS_DISK_ACK_B
        ora     #FDS_DISK_ACK_BATTERY
        sta     FDS_DISK_ACK_B
        sta     FDS_DISK_ACK
        ldx     #fds_errnos::battery
        lda     FDS_EXT_READ
        eor     #<~0
        rol     a
        jsr     fds_load_bcs_fail

        jsr     fds_io_mode_r_init
	: ; while (fds_load_bcs_fail()) {
		ldx     #fds_errnos::disk_set
		lda     FDS_DISK_SR
		lsr     a
		jsr     fds_load_bcs_fail

		lsr     a
		bcs     :-
	; }

        rts
; -----------------------------------------------------------
fds_io_mode_r_init:
	lda     FDS_CTLR_B
        and     #(fds_ctlr::mirror)
        ora     #(fds_ctlr::mask|fds_ctlr::io_mode_r|fds_ctlr::transfer_reset)
        sta     FDS_CTLR
        ora     #(fds_ctlr::motor_on)
        sta     FDS_CTLR
        and     #<~(fds_ctlr::transfer_reset)
        sta     FDS_CTLR_B
        sta     FDS_CTLR
        rts
; -----------------------------------------------------------
fds_blocktype_id	= work_07

fds_blocktype_check:
	ldy     #5
        jsr     fds_sleep_ms

        sta     fds_blocktype_id
        clc
        adc     #fds_errnos::blockerrs
        tay

        lda     FDS_CTLR_B
        ora     #(fds_ctlr::drive_ready)
        sta     FDS_CTLR_B
        sta     FDS_CTLR

        jsr     fds_irq_transfer_s
        pha

        tya
        tax

        pla
        cmp     fds_blocktype_id
        jsr     fds_tx_fail
        rts
; -----------------------------------------------------------
fds_fileblk_write:
	ldy     #10
        sta     work_07
        lda     FDS_CTLR_B
        and     #(fds_ctlr::mask|fds_ctlr::mirror|fds_ctlr::transfer_reset|fds_ctlr::motor)
        sta     FDS_CTLR
        jsr     fds_sleep_ms

        ldy     #0
        sty     FDS_DATA_WRITE
        ora     #(fds_ctlr::drive_ready)
        sta     FDS_CTLR_B
        sta     FDS_CTLR
        lda     #$80
        jsr     fds_irq_transfer_s
        lda     work_07
        jsr     fds_irq_transfer
        rts
; -----------------------------------------------------------
fds_header_str:
	.byte	"*CVH-ODNETNIN*"
fds_header_str_end:
; ----------------------------
fds_header_check:
	jsr     fds_drive_wait

        ldy     #197
        jsr     fds_sleep_ms
        ldy     #70
        jsr     fds_sleep_ms

        lda     #FDS_DISKHEADER_ID
        jsr     fds_blocktype_check

        ldy     #(fds_header_str_end-fds_header_str)-1
	: ; for (byte of fds_header_str) {
		jsr     fds_irq_transfer
		ldx     #fds_errnos::blockerr_header
		cmp     fds_header_str, y
		jsr     fds_tx_fail

		dey
		bpl     :-
	; }

        rts
; -----------------------------------------------------------
fds_blk_crc:
	jsr     fds_irq_transfer
        ldx     #fds_errnos::crcoverflow
        lda     FDS_MEDIA_SR
        and     #fds_media_sr::end_media
        bne     fds_tx_fail_do ; if (!end_media) {
		lda     FDS_CTLR_B
		ora     #(fds_ctlr::crc_ctl)
		sta     FDS_CTLR_B
		sta     FDS_CTLR

		jsr     fds_irq_transfer
		ldx     #fds_errnos::crcfail
		lda     FDS_MEDIA_SR
		and     #fds_media_sr::crc_error
		bne     fds_tx_fail_do ; if (!crc_error) {
			beq     fds_tx_check
		; } else fds_tx_fail_do();
	; } else fds_tx_fail_do();
; -----------------------------------------------------------
fds_tx_end:
	jsr     fds_irq_transfer
        ldx     #fds_errnos::txoverflow
        lda     FDS_MEDIA_SR
        and     #fds_media_sr::end_media
        bne     fds_tx_fail_do ; if (!end_media) {
		lda     FDS_CTLR_B
		ora     #(fds_ctlr::crc_ctl)
		sta     FDS_CTLR_B
		sta     FDS_CTLR

		ldx     #178
		: ; for (x = 178; x > 0; x--) {
			dex
			bne     :-
		; }

		ldx     #fds_errnos::txwait
		lda     FDS_DISK_SR
		and     #(fds_disk_sr::drive_wait)
		; if (drive_wait) {
			bne     fds_tx_fail_do
		; }
	; } else fds_tx_fail_do();
; ----------------------------
fds_tx_check:
	lda     FDS_CTLR_B
        and     #<~(fds_ctlr::irq_yes|fds_ctlr::drive_ready|fds_ctlr::crc_ctl)
        ora     #fds_ctlr::io_mode_r
        sta     FDS_CTLR_B
        sta     FDS_CTLR

        ldx     #fds_errnos::disk_set
        lda     FDS_DISK_SR
        lsr     a
        jsr     fds_load_bcs_fail
        rts
; -----------------------------------------------------------
fds_crc_read:
        jsr     fds_irq_transfer
        sta     (fds_disk_buffer), y
        ldx     #fds_errnos::crcoverflow
        lda     FDS_MEDIA_SR
        and     #fds_media_sr::end_media
        bne     fds_tx_fail_do
        iny
        jsr     fds_irq_transfer
        sta     (fds_disk_buffer), y
        jmp     fds_tx_check
; -----------------------------------------------------------
fds_load_done:
	ldx     #0
        beq     :++ ; if () {
	fds_load_bcs_fail:
		bcs     fds_tx_fail_do
		: rts
	fds_tx_fail:
		beq     :-

	fds_tx_fail_do:
		txa
		ldx     fds_disk_ptr_stack_next
		txs
		tax
	: ; }

	lda     FDS_CTLR_B
        and     #(fds_ctlr::mirror|fds_ctlr::motor)
        ora     #(fds_ctlr::mask|fds_ctlr::io_mode_r|fds_ctlr::transfer_reset)
        sta     FDS_CTLR_B
        sta     FDS_CTLR
        txa
        cli
        rts
; -----------------------------------------------------------
fds_irq_transfer_s:
	ldx     #fds_irqmode::transfer
        stx     FDS_STACK_IRQMODE

        rol     FDS_CTLR_B
        sec
        ror     FDS_CTLR_B
        ldx     FDS_CTLR_B
        stx     FDS_CTLR

fds_irq_transfer:
	cli
	: ; for (;;) {
		jmp     :-
	; }
; -----------------------------------------------------------
fds_buffer_inc:
	inc     fds_disk_buffer
        bne     :+
        	inc     fds_disk_buffer+1
	:
; ----------------------------
fds_buffer_size_dec:
	sec
        lda     fds_disk_buffer_size
        sbc     #1
        sta     fds_disk_buffer_size
        lda     fds_disk_buffer_size+1
        sbc     #0
        sta     fds_disk_buffer_size+1
        rts
