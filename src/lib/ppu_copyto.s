.include	"system/ppu.i"

.include	"mem.i"

fds_ppu_copy_to_dec_8:
	lda     #8
fds_ppu_copy_to_dec:
	php
        ldy     #0
        clc
        adc     work_00
        sta     work_00
        lda     #0
        adc     work_00+1
        sta     work_00+1
        plp
        dec     work_02
        rts
; ----------------------------
fds_ppu_copy_to_oi_line:
	ldx     #8
	: ; for (x = 8; x > 0; x--) {
		bcs     :++

		lda     (work_00), y
		sta     PPU_VRAM_IO

		: iny
		dex
		bne     :--
	; }

        rts

	: ; if (c) {
		lda     PPU_VRAM_IO
		sta     (work_00), y
		bcs     :--
	; }
; -------------
fds_ppu_copy_to_io_line:
	lda     work_03
        ldx     #8
	: ; for (x = 8, x > 0; x--) {
		bcs     :++

		sta     PPU_VRAM_IO

		: dex
		bne     :--
	; }

        rts

	: ; if (c) {
		lda     PPU_VRAM_IO
		bcs     :--
	; }
; -------------
fds_ppu_copy_to_write_line_masked:
	ldx     #8
	: ; for (x = 8; x > 0; x--) {
		lda     work_03
		eor     (work_00), y
		sta     PPU_VRAM_IO
		iny

		dex
		bne     :-
	; }
        rts
; ----------------------------
	.export fds_ppu_copy_to
fds_ppu_copy_to:
	sta     work_03+1
        stx     work_02
        sty     work_03
        jsr     fds_sys_getptr
        lda     PPU_SR
        lda     FDS_PPU_CTLR0_B
        and     #<~(ppu_ctlr0::inc_32)
        sta     FDS_PPU_CTLR0_B
        sta     PPU_CTLR0
        ldy     work_03
        sty     PPU_VRAM_AR
        lda     work_03+1
        and     #$F0
        sta     PPU_VRAM_AR
        lda     #$00
        sta     work_03
        lda     work_03+1
        and     #$0F
        lsr     a
        bcc     :+ ; if () {
		dec     work_03
	: ; }
	lsr     a
        bcc     :+ ; if () {
		ldx     PPU_VRAM_IO
	: ; }
	tay
        beq     :++
        dey
        beq     :+++
        dey
        beq     :++++
        dey
	: ; while () {
		jsr     fds_ppu_copy_to_write_line_masked
		ldy     #0
		jsr     fds_ppu_copy_to_oi_line
		jsr     fds_ppu_copy_to_dec_8
		bne     :-
	; }
        rts

	: ; while () {
		jsr     fds_ppu_copy_to_oi_line
		jsr     fds_ppu_copy_to_oi_line
		lda     #$10
		jsr     fds_ppu_copy_to_dec
		bne     :-
	; }
        rts

	: ; while () {
		jsr     fds_ppu_copy_to_oi_line
		jsr     fds_ppu_copy_to_io_line
		jsr     fds_ppu_copy_to_dec_8
		bne     :-
	; }
        rts

	: ; while () {
		jsr     fds_ppu_copy_to_io_line
		jsr     fds_ppu_copy_to_oi_line
		jsr     fds_ppu_copy_to_dec_8
		bne     :-
	; }
        rts
; -----------------------------------------------------------
	.export sys_EC22
sys_EC22_rts:
	rts

sys_EC22:
	ldy     #$0B
        lda     (work_00), y
        sta     work_02
        lda     #$02
        sta     work_02+1
        dey
        lda     (work_00), y
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        beq     sys_EC22_rts
        sta     work_04
        sta     work_0C
        lda     (work_00), y
        and     #$0F
        beq     sys_EC22_rts
        sta     work_05

        ldy     #1
        lda     (work_00), y
        tax
        dey
        lda     (work_00), y
        beq     :+
        bpl     sys_EC22_rts ; if (work[0] < 0) {
		ldx     #$F4
	: ; }
	stx     work_07+1

        ldy     #8
        lda     (work_00), y
        lsr     a
        and     #$08
        beq     :+ ; if (work[8] & 0x10) {
		lda     #$80
	: ; }
	ror     a
        sta     work_09

        iny
        lda     (work_00), y
        and     #$23
        ora     work_09
        sta     work_09
        ldy     #3
        lda     (work_00), y
        sta     work_0A

        lda     work_05
        sta     work_07
        ldy     #0
        sty     work_0B
	: ; for (j = work[5]; j > 0; j--) {
		lda     work_04
		sta     work_06
		ldx     work_07+1
		: ; for (i = work[4]; i > 0; i--) {
			txa
			sta     (work_02), y
			cmp     #$F4
			beq     :+ ; if (work[8] != 0xF4) {
				clc
				adc     #8
				tax
			: ; }

			iny
			iny
			lda     work_09
			sta     (work_02), y
			iny
			lda     work_0A
			sta     (work_02), y
			iny
			inc     work_0B

			dec     work_06
			bne     :--
		; }

		lda     work_0A
		clc
		adc     #$08
		sta     work_0A

		dec     work_07
		bne     :---
	; }

        ldy     #7
        lda     (work_07-7), y
        sta     work_07
        dey
        lda     (work_07-7), y
        sta     work_07+1
        lda     #0
        sta     work_0A
        clc
        ldx     work_0B
        dey
	: ; for (x = work[0xB]; x > 0; x--) {
		lda     (work_00), y
		clc
		adc     work_07
		sta     work_07
		lda     #0
		adc     work_07+1
		sta     work_07+1

		dex
		bne     :-
	; }

        inc     work_02

        ldy     #0
        lda     work_07+1
        bne     :+ ; if (work_07 < 0x100) {
		dec     work_0A
		ldy     work_07
	: ; }

	bit     work_09
        bmi     :++++
        bvs     :+++++ ; if () {
		: ; for () {
			lda     (work_07), y
			bit     work_0A
			bpl     :+ ; if (work_07 < 0x100) {
				tya
			: ; }
			sta     (work_02, x)

			dey
			bit     work_09
			bmi     :+ ; if (work[9] < 0) {
				iny
				iny
			: ; }

			lda     #4
			clc
			adc     work_02
			sta     work_02
			dec     work_0B
			bne     :---
		; }

		rts
	: bvc     :++
	: ; } else if ((work[9] & 0x40)) {
		tya
		clc
		adc     work_0B
		tay
		dey
		bit     work_09
		bmi     :-----

		lda     #$FF
		eor     work_0C
		sta     work_0C
		inc     work_0C
	; }

	: ; for (work[5]; work[5] > 0; work[5]--) {
		tya
		clc
		adc     work_0C
		tay

		lda     work_04
		sta     work_06
		: ; for (i = work[4]; i > 0; i--) {
			dey
			bit     work_09
			bmi     :+ ; if (work[9] >= 0) {
				iny
				iny
			: ; }

			lda     (work_07), y
			bit     work_0A
			bpl     :+ ; if (work_07 < 0x100) {
				tya
			: ; }

			sta     (work_02, x)
			lda     #4
			clc
			adc     work_02
			sta     work_02

			dec     work_06
			bne     :---
		; }

		tya
		clc
		adc     work_0C
		tay

		dec     work_05
		bne     :----
	; }

        rts
