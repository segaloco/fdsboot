.include	"mem.i"

	.export fds_rand
fds_rand:
	lda     work_00, x
        and     #%00000010
        sta     work_00

        lda     work_00+1, x
        and     #%00000010
        eor     work_00 ; if (!((work_00[x] ^ work_01[x]) & %00000010)) {
        	clc
        beq     :+ ; } else {
		sec
	; }
	: ; for (y; y > 0; y--) {
		ror     work_00, x

		inx
		dey
		bne     :-
	; }

        rts
