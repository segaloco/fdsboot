.include	"system/ppu.i"

.include	"mem.i"

ppu_vram_ar_hi		= work_00
ppu_vram_fill_chr	= work_01
ppu_vram_fill_attr	= work_02

	.export fds_ppu_fill
fds_ppu_fill:
	sta     ppu_vram_ar_hi
        stx     ppu_vram_fill_chr
        sty     ppu_vram_fill_attr
        lda     PPU_SR
        lda     FDS_PPU_CTLR0_B
        and     #<~(ppu_ctlr0::inc_32)
        sta     PPU_CTLR0
        sta     FDS_PPU_CTLR0_B

        lda     ppu_vram_ar_hi
        sta     PPU_VRAM_AR
        ldy     #$00
        sty     PPU_VRAM_AR
        ldx     #$04
        cmp     #>PPU_VRAM_BG
        bcs     :+ ; if (ppu_vram_ar < PPU_VRAM_BG1) {
		ldx     ppu_vram_fill_attr
	: ; }
	ldy     #0
        lda     ppu_vram_fill_chr
	: ; for (y = 0x100, x = (work[0] < 0x20) ? (work[2]) : (4); y > 0 && x > 0; y--, x--) {
		sta     PPU_VRAM_IO

		dey
		bne     :-
		dex
		bne     :-
	; }

        ldy     ppu_vram_fill_attr
        lda     ppu_vram_ar_hi
        cmp     #$20
        bcc     :++ ; if (work[0] >= 0x20) {
		adc     #$02
		sta     PPU_VRAM_AR
		lda     #$C0
		sta     PPU_VRAM_AR
		ldx     #$40
		: ; for (x = 0x40; x > 0; x--) {
			sty     PPU_VRAM_IO

			dex
			bne     :-
		; }
	: ; }

	ldx     ppu_vram_fill_chr
        rts
