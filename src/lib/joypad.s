.include	"system/ctrl.i"

.include	"mem.i"

fds_joypad_bits_1	= work_00
fds_joypad_bits_2	= work_00+1

	.export	fds_joypad_read
fds_joypad_read:
	ldx     FDS_CTRL0_B
        inx
        stx     CTRL0
        dex
        stx     CTRL0

        ldx     #CTRL_BIT_COUNT
	: ; for (bit of ctrl) {
		lda     CTRL0
		lsr     a
		rol     FDS_JOYPAD_PRESS_1
		lsr     a
		rol     fds_joypad_bits_1
		lda     CTRL1
		lsr     a
		rol     FDS_JOYPAD_PRESS_2
		lsr     a
		rol     fds_joypad_bits_2

		dex
		bne     :-
	; }

        rts
; -----------------------------------------------------------
fds_joypad_or:
	lda     fds_joypad_bits_1
        ora     FDS_JOYPAD_PRESS_1
        sta     FDS_JOYPAD_PRESS_1
        lda     fds_joypad_bits_2
        ora     FDS_JOYPAD_PRESS_2
        sta     FDS_JOYPAD_PRESS_2
        rts
; -----------------------------------------------------------
	.export fds_joypad_stat
fds_joypad_stat:
	jsr     fds_joypad_read
        beq     :+ ; if (fds_joypad_read()) {
		jsr     fds_joypad_read
		jsr     fds_joypad_or
	: ; }

	: ; for () {
		; for () {
			ldx     #2-1
			: ; for (x = 2-1; x >= 0; x--) {
				lda     FDS_JOYPAD_PRESS, x
			
				.export	fds_joypad_stat_do
			fds_joypad_stat_do:
				tay
				eor     FDS_JOYPAD_HOLD, x
				and     FDS_JOYPAD_PRESS, x
				sta     FDS_JOYPAD_PRESS, x
				sty     FDS_JOYPAD_HOLD, x
				dex
				bpl     :-
			; }

			rts

			jsr     fds_joypad_read
			: ; for (FDS_JOYPAD_PRESS_1) {
				ldy     FDS_JOYPAD_PRESS_1
				lda     FDS_JOYPAD_PRESS_2
				pha
				jsr     fds_joypad_read
				pla
				cmp     FDS_JOYPAD_PRESS_2
				bne     :-
				cpy     FDS_JOYPAD_PRESS_1
				bne     :-
				beq     :---
			; }
		; }

		jsr     fds_joypad_read
		jsr     fds_joypad_or
		: ; for (FDS_JOYPAD_PRESS_1) {
			ldy     FDS_JOYPAD_PRESS_1
			lda     FDS_JOYPAD_PRESS_2
			pha
			jsr     fds_joypad_read
			jsr     fds_joypad_or
			pla
			cmp     FDS_JOYPAD_PRESS_2
			bne     :-
			cpy     FDS_JOYPAD_PRESS_1
			bne     :-
			beq     :----
		; }
	; }

        jsr     fds_joypad_read
        lda     fds_joypad_bits_1
        sta     FDS_JOYPAD_HOLD_1
        lda     fds_joypad_bits_2
        sta     FDS_JOYPAD_HOLD_2

        ldx     #4-1
	: ; for (x = 4-1; x >= 0; x--) {
		lda     FDS_JOYPAD_PRESS, x
		tay
		eor     work_tbl_F1, x
		and     FDS_JOYPAD_PRESS, x
		sta     FDS_JOYPAD_PRESS, x
		sty     work_tbl_F1, x

		dex
		bpl     :-
	; }

        rts
