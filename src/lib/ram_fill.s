.include	"mem.i"

	.export fds_ram_fill
fds_ram_fill:
	pha
        txa
        sty     work_00+1
        clc
        sbc     work_00+1
        tax
        pla

        ldy     #0
        sty     work_00
	: ; for (x, y = 0; x < 0x100 && y > 0; x++, y--) {
		sta     (work_00), y
		dey
		bne     :-
		dec     work_00+1
		inx
		bne     :-
	; }

        rts
