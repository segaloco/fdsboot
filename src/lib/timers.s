.include	"system/ppu.i"
.include	"system/ctrl.i"
.include	"system/fds.i"

.include	"tunables.i"
.include	"mem.i"

fds_timers_base	= work_00

	.export fds_timers_tick
fds_timers_tick:
	stx     fds_timers_base
        dec     RAM_BASE, x
        bpl     :+ ; if (--work_00[x] < 0) {
		lda     #9
		sta     RAM_BASE, x
		tya
	: ; }

	tax
	: ; for (x = () ? y : a; x > 0; x--) {
		lda     RAM_BASE, x
		beq     :+ ; if (*x != 0) {
			dec     RAM_BASE, x
		: ; }

		dex
		cpx     fds_timers_base
		bne     :--
	; }

        rts
