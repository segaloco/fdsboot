.include	"system/ppu.i"

.include	"mem.i"

	.export fds_set_obj_dma
fds_set_obj_dma:
	lda     #<(oam_buffer)
        sta     PPU_OAM_AR
        lda     #>(oam_buffer)
        sta     OBJ_DMA
        rts
