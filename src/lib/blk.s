.include	"system/ppu.i"
.include	"system/fds.i"

	.export fds_ppuctlr1_blk_on, fds_ppuctlr1_blk_off
fds_ppuctlr1_blk_on:
	lda     FDS_PPU_CTLR1_B
        and     #<~(ppu_ctlr1::objblk_off|ppu_ctlr1::bgblk_off)

fds_ppuctlr1_set_end:
	sta     FDS_PPU_CTLR1_B
	sta     PPU_CTLR1
        rts

fds_ppuctlr1_blk_off:
	lda     FDS_PPU_CTLR1_B
        ora     #(ppu_ctlr1::objblk_off|ppu_ctlr1::bgblk_off)
        bne     fds_ppuctlr1_set_end

	.export fds_ppuctlr1_objblk_on, fds_ppuctlr1_objblk_off
fds_ppuctlr1_objblk_on:
	lda     FDS_PPU_CTLR1_B
        and     #<~(ppu_ctlr1::objblk_off)
        jmp     fds_ppuctlr1_set_end

fds_ppuctlr1_objblk_off:
        lda     FDS_PPU_CTLR1_B
        ora     #(ppu_ctlr1::objblk_off)
        bne     fds_ppuctlr1_set_end

	.export fds_ppuctlr1_bgblk_on, fds_ppuctlr1_bgblk_off
fds_ppuctlr1_bgblk_on:
	lda     FDS_PPU_CTLR1_B
        and     #<~(ppu_ctlr1::bgblk_off)
        jmp     fds_ppuctlr1_set_end

fds_ppuctlr1_bgblk_off:
	lda     FDS_PPU_CTLR1_B
        ora     #(ppu_ctlr1::bgblk_off)
        bne     fds_ppuctlr1_set_end
