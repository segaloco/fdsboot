.include	"mem.i"

tbljmp_tbl_ptr		= work_00
tbljmp_target_ptr	= work_00

	.export fds_tbljmp
fds_tbljmp:
        asl     a
        tay
        iny
        pla
        sta     tbljmp_tbl_ptr
        pla
        sta     tbljmp_tbl_ptr+1
        lda     (tbljmp_tbl_ptr), y
        tax
        iny
        lda     (tbljmp_tbl_ptr), y
        sta     tbljmp_target_ptr+1
        stx     tbljmp_target_ptr
        jmp     (tbljmp_target_ptr)
