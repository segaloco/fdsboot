.include	"system/ppu.i"

.include	"mem.i"

	.export fds_ppu_restore
fds_ppu_restore:
	lda     PPU_SR
        lda     FDS_PPU_SCC_H_B
        sta     PPU_SCC_H_V
        lda     FDS_PPU_SCC_V_B
        sta     PPU_SCC_H_V
        lda     FDS_PPU_CTLR0_B
        sta     PPU_CTLR0
        rts
