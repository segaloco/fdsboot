.include	"system/ppu.i"
.include	"system/ctrl.i"
.include	"system/fds.i"

.include	"tunables.i"
.include	"mem.i"

ppu_displist_write_addr	= work_00
; -----------------------------------------------------------
	.export fds_ppu_displist_write
fds_ppu_displist_write:
	jsr	fds_sys_getptr

	jmp	fds_ppu_displist_write_loop
	fds_ppu_il_proc: ; for () {
		pha

		sta	PPU_VRAM_AR
		iny
		lda	(ppu_displist_write_addr), y
		sta	PPU_VRAM_AR

		iny
		lda	(ppu_displist_write_addr), y
		asl	a
		pha

		lda	FDS_PPU_CTLR0_B
		ora	#ppu_ctlr0::inc_32
		bcs	:+ ; if () {
			and	#<~(ppu_ctlr0::inc_32)
		: ; }
		sta	PPU_CTLR0
		sta	FDS_PPU_CTLR0_B

		pla
		asl	a
		php

		bcc	:+ ; if ((this.length & DISPLIST_REPEAT)) {
			ora	#1<<1
			iny
		: ; }

		plp ; if () {
			clc
		bne	:+ ; } else {
			sec
		: ; }
		ror	a
		lsr	a
		tax
		: ; for () {
			bcs	:+ ; if () {
				iny
			: ; }

			lda	(ppu_displist_write_addr), y
			sta	PPU_VRAM_IO

			dex
			bne	:--
		; }

		pla
		cmp	#$3F
		bne	:+ ; if () {
			sta	PPU_VRAM_AR
			stx	PPU_VRAM_AR
			stx	PPU_VRAM_AR
			stx	PPU_VRAM_AR
		: ; }

		; for () {
			sec
			tya
			adc	ppu_displist_write_addr
			sta	ppu_displist_write_addr
			lda	#0
			adc	ppu_displist_write_addr+1
			sta	ppu_displist_write_addr+1
			fds_ppu_displist_write_loop: ; for () {
				ldx	PPU_SR
				ldy	#0
				lda	(ppu_displist_write_addr), y
				bpl	:+ ; if (ppu_displist_write_addr[0] < 0) {
					rts
				: cmp	#CPU_OPCODE_RTS
				bne	:+ ; } else if (work[0] == rts) {
					pla
					sta	ppu_displist_write_addr+1
					pla
					sta	ppu_displist_write_addr
					ldy	#2
					bne	:--
				: cmp	#CPU_OPCODE_JMP
				bne	fds_ppu_il_proc ; } else if (work[0] == jmp) {
					lda	ppu_displist_write_addr
					pha
					lda	ppu_displist_write_addr+1
					pha
					iny
					lda	(ppu_displist_write_addr), y
					tax
					iny
					lda	(ppu_displist_write_addr), y
					sta	ppu_displist_write_addr+1
					stx	ppu_displist_write_addr
					bcs	fds_ppu_displist_write_loop
				; }
			; }
		; }
	; }

fds_sys_getptr_stackoff		= STACK_BASE+3
fds_sys_getptr_calling_pc	= work_05

	.export	fds_sys_getptr
fds_sys_getptr:
	tsx
	lda	fds_sys_getptr_stackoff, x
	sta	fds_sys_getptr_calling_pc
	lda	fds_sys_getptr_stackoff+1, x
	sta	fds_sys_getptr_calling_pc+1
	ldy	#1
	lda	(fds_sys_getptr_calling_pc), y
	sta	ppu_displist_write_addr
	iny
	lda	(fds_sys_getptr_calling_pc), y
	sta	ppu_displist_write_addr+1
	clc
	lda	#WORD_SIZE
	adc	fds_sys_getptr_calling_pc
	sta	fds_sys_getptr_stackoff, x
	lda	#0
	adc	fds_sys_getptr_calling_pc+1
	sta	fds_sys_getptr_stackoff+1, x
	rts
; -----------------------------------------------------------
	.export fds_ppu_inc_load
fds_ppu_inc_load:
	lda	FDS_PPU_CTLR0_B
	and	#<~(ppu_ctlr0::inc_32)
	sta	PPU_CTLR0
	sta	FDS_PPU_CTLR0_B
	ldx	PPU_SR
	ldy	#0
	beq	:++++
	: ; while (!(ppu_displist[i].byte & 0x80)) {
		pha
		sta	PPU_VRAM_AR
		iny
		lda	ppu_displist+(PPU_VRAM_TBL_ENTRY_SIZE-2)+PPU_VRAM_TBL_AR_LO, y
		sta	PPU_VRAM_AR
		iny

		ldx	ppu_displist+PPU_VRAM_TBL_AR_BYTE, y
		: ; for (x = ppu_displist[i].byte; x > 0; x--) {
			iny
			lda	ppu_displist+PPU_VRAM_TBL_AR_BYTE, y
			sta	PPU_VRAM_IO

			dex
			bne	:-
		; }

		pla
		cmp	#$3F
		bne	:+ ; if (a == 0x3F) {
			sta	PPU_VRAM_AR
			stx	PPU_VRAM_AR
			stx	PPU_VRAM_AR
			stx	PPU_VRAM_AR
		: ; }

		iny
	
		: lda	ppu_displist+PPU_VRAM_TBL_AR_BYTE, y
		bpl	:----
	; }

	sta	ppu_displist+PPU_VRAM_TBL_AR_BYTE
	lda	#0
	sta	ppu_displist+PPU_VRAM_TBL_AR_LO
	rts
; -----------------------------------------------------------
fds_ppu_vram_read:
	lda	PPU_SR
	: ; for (y; y > 0; y--) {
		lda	ppu_displist+PPU_VRAM_TBL_AR_HI, x
		sta	PPU_VRAM_AR
		inx
		lda	ppu_displist+PPU_VRAM_TBL_AR_LO-1, x
		sta	PPU_VRAM_AR
		inx
		lda	PPU_VRAM_IO
		lda	PPU_VRAM_IO
		sta	ppu_displist+PPU_VRAM_TBL_AR_BYTE-2, x
		inx

		dey
		bne	:-
	; }

	rts
; -----------------------------------------------------------
fds_ppu_buffer_load_addr	= work_02
fds_ppu_buffer_load_count	= work_04

	.export fds_ppu_buffer_load
fds_ppu_buffer_load:
	sta	fds_ppu_buffer_load_addr+1
	stx	fds_ppu_buffer_load_addr

	sty	fds_ppu_buffer_load_count

	jsr	fds_sys_getptr

	ldy	#$FF
	lda	#1
	bne	fds_ppu_buffer_load_do
; --------------
	.export	fds_ppu_buffer_tbl_load
fds_ppu_buffer_tbl_load:
	sta	fds_ppu_buffer_load_addr+1
	stx	fds_ppu_buffer_load_addr

	jsr	fds_sys_getptr

	ldy	#0
	lda	(ppu_displist_write_addr), y
	and	#%00001111
	sta	fds_ppu_buffer_load_count

	lda	(ppu_displist_write_addr), y
	lsr	a
	lsr	a
	lsr	a
	lsr	a
; --------------
fds_ppu_buffer_load_do:
	sta	work_05
	ldx	ppu_displist+PPU_VRAM_TBL_AR_LO
	: ; for (i = 1; i > 0; i--) {
		lda	fds_ppu_buffer_load_addr+1
		sta	ppu_displist+PPU_VRAM_TBL_AR_BYTE, x
		jsr	fds_ppu_buffer_load_inx
		lda	fds_ppu_buffer_load_addr
		sta	ppu_displist+PPU_VRAM_TBL_AR_BYTE, x
		jsr	fds_ppu_buffer_load_inx

		lda	fds_ppu_buffer_load_count
		sta	work_06
		sta	ppu_displist+PPU_VRAM_TBL_AR_BYTE, x
		: ; for (entry of buffer) {
			jsr	fds_ppu_buffer_load_inx
			iny
			lda	(ppu_displist_write_addr), y
			sta	ppu_displist+PPU_VRAM_TBL_AR_BYTE, x

			dec	work_06
			bne	:-
		; }

		jsr	fds_ppu_buffer_load_inx
		stx	ppu_displist+PPU_VRAM_TBL_AR_LO
		clc
		lda	#>PPU_VRAM_BG
		adc	fds_ppu_buffer_load_addr
		sta	fds_ppu_buffer_load_addr
		lda	#<PPU_VRAM_BG
		adc	fds_ppu_buffer_load_addr+1
		sta	fds_ppu_buffer_load_addr+1

		dec	work_05
		bne	:--
	; }

	lda	#$FF
	sta	ppu_displist+PPU_VRAM_TBL_AR_BYTE, x
	rts
; ----------------------------
fds_ppu_buffer_load_inx:
	inx
	cpx	ppu_displist+PPU_VRAM_TBL_AR_HI
	bcc	:+ ; if (++x > ppu_displist.ar_hi) {
		ldx	ppu_displist+PPU_VRAM_TBL_AR_LO
		lda	#$FF
		sta	ppu_displist+PPU_VRAM_TBL_AR_BYTE, x
		pla
		pla
		lda	#1
	: ; }

	rts
; ----------------------------
fds_ppu_buffer_load_dex:
	dex
	dex
	dex
	txa
	: ; for (y; y > 0; y--) {
		clc
		adc	#PPU_VRAM_TBL_ENTRY_SIZE

		dey
		bne	:-
	; }
	tax
	tay
	lda	ppu_displist+PPU_VRAM_TBL_AR_HI, x
	cmp	ppu_displist_write_addr
	bne	:+
	inx
	lda	ppu_displist+PPU_VRAM_TBL_AR_HI, x
	cmp	ppu_displist_write_addr+1
	bne	:+ ; if () {
		inx
		lda	ppu_displist+PPU_VRAM_TBL_AR_HI, x
		clc
		rts
	: ; } else {
		lda	ppu_displist_write_addr
		sta	ppu_displist+PPU_VRAM_TBL_AR_HI, y
		iny
		lda	ppu_displist_write_addr+1
		sta	ppu_displist+PPU_VRAM_TBL_AR_HI, y
		sec
		rts
	; }
