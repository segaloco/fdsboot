.include	"mem.i"
.include	"tunables.i"

	.export	fds_sleep_short, fds_sleep_ms
fds_sleep_short:
	pha

	lda	#FDS_SLEEP_SHORT_TIME
	sec
	: ; for (a = FDS_SLEEP_SHORT_TIME; a >= 0; a--) {
		sbc	#FDS_SLEEP_SHORT_DEC
		bcs	:-
	; }

	pla 
	rts 

fds_sleep_ms:
	: ; for (y; y > 0; y--) {
		ldx	work_00

		ldx	#FDS_SLEEP_LONG_TIME
		: ; for (x = FDS_SLEEP_LONG_TIME; x > 0; x--) {
			nop

			dex
			bne	:-
		; }

		cmp     work_00

		dey
		bne	:--
	; }

        rts
