.include	"system/apu.i"

.include	"mem.i"

	.export snd_FF5C
; -----------------------------------------------------------
snd_FE36:
	lsr     a
        bcs     LFE66
        lsr     work_E1
        bcs     LFE47
        lsr     a
        bcs     LFEA6
        lsr     work_E1
        bcs     LFE7A ; if () {
        	jmp     snd_FF6A
	LFE47: ; } else if () {
			lda     #$10
			sta     PULSE1_VOL
			lda     #$01
			sta     TRIANGLE_VOL
			sty     work_E3
			lda     #$20
			sta     work_E4
			ldx     #$5C
			ldy     #$7F
			stx     PULSE2_VOL
			sty     PULSE2_SWEEP
			lda     #$F9
			sta     PULSE2_COUNTER
		LFE66:
		lda     work_E4
		lsr     a
		bcc     LFE6F
			lda     #$0D
			bne     LFE71
		LFE6F:
			lda     #$7C
		LFE71:
		sta     PULSE2_TIMER

		jmp     snd_FFC6
	LFE77: ; } else if () {
		jmp     snd_FFCA
	LFE7A: ; } else {
			sty     work_E3
			ldx     #$9C
			ldy     #$7F
			stx     PULSE1_VOL
			stx     PULSE2_VOL
			sty     PULSE1_SWEEP
			sty     PULSE2_SWEEP
			lda     #$20
			sta     TRIANGLE_VOL
			lda     #$01
			sta     NOISE_VOL
			ldx     #$00
			stx     work_E9
			stx     work_EA
			stx     work_EB
			lda     #$01
			sta     work_E6
			sta     work_E7
			sta     work_E8
		LFEA6:
		dec	work_E6
		bne	:+ ; if () {
			ldy	work_E9
			iny
			sty	work_E9
			lda	tbl_FF1F, y
			beq	LFE77 ; if () {
				jsr	snd_FFE9
				sta	work_E6
				txa
				and	#$3E
				ldx	#$04
				jsr	snd_FFD9
			; }
		: ; }

		dec     work_E7
		bne     :+ ; if () {
			ldy     work_EA
			iny
			sty     work_EA
			lda	tbl_FF33, y
			jsr     snd_FFE9
			sta     work_E7
			txa
			and     #$3E
			ldx     #$00
			jsr     snd_FFD9
		: ; }

		dec     work_E8
		bne     :+ ; if () {
			lda     #$09
			sta     NOISE_PERIOD
			lda     #$08
			sta     NOISE_COUNTER
			ldy     work_EB
			iny
			sty     work_EB
			lda     tbl_FF46, y
			jsr     snd_FFE9
			sta     work_E8
			txa
			and     #$3E
			ldx     #$08
			jsr     snd_FFD9
		: ; }

		jmp	snd_FF6A
	; }
; ----------------------------
tbl_FF00:
	.byte   $03, $57
	.byte	$00, $00
	.byte	$08, $D4
	.byte	$08, $BD
	.byte	$08, $B2
	.byte	$09, $AB
	.byte	$09, $7C
	.byte	$09, $3F
	.byte	$09, $1C
	.byte	$08, $FD
	.byte	$08, $EE
	.byte	$09, $FC
	.byte	$09, $DF

tbl_FF1A:
	.byte	$06, $0C, $12, $18, $08

tbl_FF1F:
	.byte	$48, $CA
	.byte	$CE, $D4
	.byte	$13, $11
	.byte	$0F, $90
	.byte	$10, $C4
	.byte	$C8, $07
	.byte	$05, $15
	.byte	$C4, $D2
        .byte   $D4, $8E
	.byte	$0C, $4F

tbl_FF33:
	.byte	$00, $D6
	.byte	$D6, $CA
        .byte   $0B, $19
	.byte	$17, $98
	.byte	$18, $CE
	.byte	$D4, $15
        .byte   $13, $11
	.byte	$D2, $CA
	.byte	$CC, $96
	.byte	$18

tbl_FF46:
	.byte   $57, $CE
	.byte	$0F, $0F
        .byte   $0F, $CE
	.byte	$CE, $CE
	.byte	$CE, $CE
	.byte	$0F, $0F
        .byte   $0F, $CE
	.byte	$CE, $CE
	.byte	$CE, $CE
	.byte	$0F, $0F
        .byte   $0F, $CE
; -----------------------------------------------------------
	.export snd_FF5C
snd_FF5C:
	ldy     work_E1
        lda     work_E3
        lsr     work_E1
        bcs     snd_FF7B
        lsr     a
        bcs     snd_FF9A
        jmp     snd_FE36
; ----------------------------
snd_FF6A:
	lda     #$00
        sta     work_E1
        rts
; ----------------------------
tbl_FF6F:	.byte	$06, $0C, $12
tbl_FF72:	.byte   $47, $5F, $71
tbl_FF75:	.byte   $5F, $71, $8E
tbl_FF78:	.byte	$71, $8E, $BE
; ----------------------------
snd_FF7B:
	sty     work_E3
        lda     #$12
        sta     work_E4
        lda     #$02
        sta     work_E5
        ldx     #$9F
        ldy     #$7F
        stx     PULSE1_VOL
        stx     PULSE2_VOL
        sty     PULSE1_SWEEP
        sty     PULSE2_SWEEP
        lda     #$20
        sta     TRIANGLE_VOL

snd_FF9A:
	lda     work_E4
        ldy     work_E5
        cmp     tbl_FF6F, y
        bne     :++ ; if () {
		lda     tbl_FF72, y
		sta     PULSE1_TIMER
		ldx     #$58
		stx     PULSE1_COUNTER
		lda     tbl_FF75, y
		sta     PULSE2_TIMER
		stx     PULSE2_COUNTER
		lda     tbl_FF78, y
		sta     TRIANGLE_TIMER
		stx     TRIANGLE_COUNTER

		lda     work_E5
		beq     :+ ; if () {
			dec     work_E5
		: ; }
	: ; }

snd_FFC6:
	dec     work_E4
        bne     :+ ; if () {
	snd_FFCA:
		lda     #$00
		sta     work_E3
		lda     #$10
		sta     PULSE1_VOL
		sta     PULSE2_VOL
	: ; }

	jmp     snd_FF6A
; -----------------------------------------------------------
snd_FFD9:
	tay
        lda     tbl_FF00+1, y
        beq     :+ ; if () {
		sta     PULSE1_TIMER, x
		lda	tbl_FF00, y
		sta     PULSE1_COUNTER, x
	: ; }

	rts
; -----------------------------------------------------------
snd_FFE9:
	tax
        ror     a
        txa
        rol     a
        rol     a
        rol     a
        and     #$07
        tay
        lda     tbl_FF1A, y
        rts
