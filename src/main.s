.include	"system/ppu.i"
.include	"system/ctrl.i"
.include	"system/apu.i"
.include	"system/fds.i"
.include	"system/fds_charmap.i"

.include	"mem.i"
.include	"joypad.i"
.include	"tunables.i"

.segment "VECTORS"
	.addr	nmi
	.addr	start
	.addr	irq
; -----------------------------------------------------------
	.code
security_code:
	.byte	"           NINTENDO r           "
	.byte	"       FAMILY COMPUTER TM       "
	.byte	"                                "
	.byte	"  THIS PRODUCT IS MANUFACTURED  "
	.byte	"  AND SOLD BY NINTENDO CO,LTD.  "
	.byte	"  OR BY OTHER COMPANY UNDER     "
	.byte	"  LICENSE OF NINTENDO CO,LTD..  "
; ----------------------------
	.export start
start:
	sei

        lda     #(ppu_ctlr0::sprite_8|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1|ppu_ctlr0::bg1)
        sta     PPU_CTLR0
        sta     FDS_PPU_CTLR0_B
        cld
        lda     #(ppu_ctlr1::objblk_on|ppu_ctlr1::bgblk_on|ppu_ctlr1::objlblk_off|ppu_ctlr1::bglblk_off|ppu_ctlr1::color)
        sta     FDS_PPU_CTLR1_B
        sta     PPU_CTLR1
        ldx     #2
	: ; for (x = 2; x > 0; x--) {
		: ; while (!(PPU_SR & int)) {
			lda     PPU_SR
			bpl     :-
		; }

		dex
		bne     :--
	; }

        stx     FDS_TIMER_CTLR
        stx     FDS_IO_ENABLE
        lda     #(fds_io::mask|fds_io::sound|fds_io::disk)
        sta     FDS_IO_ENABLE

        stx     FDS_PPU_SCC_H_B
        stx     FDS_PPU_SCC_V_B
        stx     FDS_CTRL0_B
        stx     CTRL0

        lda     #(fds_ctlr::mask|fds_ctlr::mirror_h|fds_ctlr::io_mode_r|fds_ctlr::transfer_reset|fds_ctlr::motor_off)
        sta     FDS_CTLR_B
        sta     FDS_CTLR
        lda     #$FF
        sta     FDS_DISK_ACK_B
        sta     FDS_DISK_ACK

        stx     DMCFLAG
        lda     #$C0
        sta     DMCFRAMECOUNT
        lda     #$0F
        sta     DMCSTATUS

        lda     #fds_snd_vol_env::off
        sta     FDS_SND_VOL
        lda     #$E8
        sta     FDS_SND_ENV_SPD

        ldx     #$FF
        txs

        lda     #fds_nmimode::game_vec_2
        sta     FDS_STACK_NMIMODE
        lda     #fds_irqmode::ack_dly
        sta     FDS_STACK_IRQMODE

        lda     FDS_STACK_RESET_F
        cmp     #FDS_RESET_READY
        bne     :++
        lda     FDS_STACK_RESET_T
        cmp     #FDS_RESET_SOFT
        beq     :+
        cmp     #FDS_RESET_FIRSTBOOT
        bne     :++ ; if (FDS_STACK_RESET_F == FDS_RESET_READY && FDS_STACK_RESET_T.in(FDS_RESET_SOFT, FDS_RESET_FIRSTBOOT)) {
		; if (FDS_STACK_RESET_T == FDS_RESET_FIRSTBOOT) {
			lda     #FDS_RESET_SOFT
			sta     FDS_STACK_RESET_T
		: ; }

	start_fdsstart:
		jsr     fds_ppu_restore

		cli
		jmp     (FDSSTART)
	: ; }

	lda     #0
        ldx     #work_end-work
	: ; for (x = work_size; x > 0; x--) {
		sta     work, x

		dex
		bne     :-
	; }

        sta     ppu_displist+PPU_VRAM_TBL_AR_LO
        lda     #$7D
        sta     ppu_displist+PPU_VRAM_TBL_AR_HI
        lda     #$FF
        sta     ppu_displist+PPU_VRAM_TBL_AR_BYTE
        jsr     fds_joypad_stat

        lda     FDS_JOYPAD_HOLD_1
        cmp     #joypad_button::select|joypad_button::start
        bne     :+ ; if (FDS_JOYPAD_HOLD_1_select | FDS_JOYPAD_HOLD_1_start) {
		lda     #FDS_RESET_HOLD
		sta     FDS_STACK_RESET_F
		jmp     sub_F4F9
	: ; }

	jsr     sub_F427
        jsr     sub_F0F0
        lda     #$4A
        sta     work_tbl_A0+$01
        lda     #$30
        sta     work_tbl_A0+$11
        lda     #$E4
        sta     work_tbl_80+$03
        lda     #$A9
        sta     FDS_PPU_SCC_V_B

        lda     FDS_DISK_SR
        and     #fds_disk_sr::drive_empty
        beq     :+ ; if (FDS_DISK_SR & drive_empty) {
		lda     #$04
		sta     work_E1
	: ; }

	: ; while (FDS_DISK_SR & drive_empty) {
		lda     #$34
		sta     work_tbl_80+$10
		: ; for (work_tbl_80[0x10] = 0x34; work_tbl_80[0x10] > 0; ???) {
			jsr     sub_F3A3

			jsr     fds_nmi_wait

			lda     work_tbl_80+$10
			cmp     #$32
			bne     :+ ; if (work_tbl_80[0x10] == 0x32) {
				lda     #1
				sta     work_E1
			: ; }

			jsr     sub_F0A7

			jsr     fds_ppu_restore
			jsr     fds_ppuctlr1_blk_off

			jsr     sub_EFDB

			ldx     #$60
			ldy     #$20
			jsr     fds_rand

			jsr     sub_F136
			jsr     sub_F36F
			ldx     #0
			jsr     sub_F212
			ldx     #$10
			jsr     sub_F212

			lda     #<work_tbl_C0
			sta     work_00
			lda     #>work_tbl_C0
			sta     work_00+1
			jsr     sys_EC22
			lda     #<(work_tbl_C0+16)
			sta     work_00
			jsr     sys_EC22

			lda     FDS_DISK_SR
			and     #fds_disk_sr::drive_empty
			bne     :--- ; if (!(FDS_DISK_SR & drive_empty)) {
				lda     FDS_PPU_SCC_V_B
				beq     :+ ; if (FDS_PPU_SCC_V_B) {
					lda     #1
					sta     FDS_PPU_SCC_V_B
				: ; }

				lda     work_tbl_80+$10
				bne     :---
			; } else break;
		; }
	; }

        jsr     fds_ppuctlr1_objblk_on
        jsr     fds_nmi_wait

        jsr     fds_ppu_displist_write
	.addr	tbl_EFF2
        jsr     fds_ppu_displist_write
	.addr	tbl_F00F
	jsr	fds_ppu_restore

        jsr     fds_filetbl_load
	.addr	tbl_EFE8
	.addr	tbl_EFE8
        bne     :+
        jsr     sub_F45E
        beq     :+++++ ; if (fds_filetbl_load(tbl_EFE8, tbl_EFE8) || sub_F45E()) {
		; if (!fds_filetbl_load(tbl_EFE8, tbl_EFE8)) {
			jsr     sub_F5FC
			lda     #$20
		: ; }
		sta     $23

		jsr     sub_F427
		jsr     sub_F0D4
		jsr     sub_F0DA
		jsr     sub_F0E0
		jsr     sub_F16C

		lda     #$10
		sta     work_tbl_A0+$03

		lda     $22
		beq     :+ ; if () {
			lda     #$01
			sta     work_tbl_80+$03
			dec     $21
		: ; }
		: ; for (work_tbl_A0[3] = 0x10; work_tbl_A0[3] > 0; ???) {
			jsr     sub_F3A3

			jsr     fds_nmi_wait

			jsr     fds_ppu_inc_load
			jsr     fds_ppu_restore
			jsr     fds_ppuctlr1_bgblk_off

			jsr     sub_EFDB
			lda     #$02
			sta     work_E1

			lda     work_tbl_A0+$03
			bne     :-
		; }

		: ; while (!(FDS_DISK_SR & drive_empty)) {
			lda     FDS_DISK_SR
			and     #(fds_disk_sr::drive_empty)
			beq     :-
		; }
		jmp     start
	: ; } else {
		lda     #$20
		sta     work_tbl_A0+$02
		: ; for (work_tbl_A0[2] = 0x20; work_tbl_A0[2] > 0; ???) {
			jsr     fds_nmi_wait

			jsr     fds_ppu_restore
			jsr     fds_ppuctlr1_bgblk_off

			ldx     FDS_PPU_SCC_V_B
			inx
			inx
			cpx     #$B0
			bcs     :+ ; if ((FDS_PPU_SCC_V_B + 2) < 0xB0) {
				stx     FDS_PPU_SCC_V_B
			: ; }

			jsr     sub_EFDB

			lda     work_tbl_A0+$02
			bne     :--
		; }

		lda     #FDS_RESET_READY
		sta     FDS_STACK_RESET_F
		lda     #FDS_RESET_FIRSTBOOT
		sta     FDS_STACK_RESET_T
		jsr     fds_ppuctlr1_bgblk_on

		ldy     #$07
		jsr     sub_F4B9
		lda     #0
		sta     FDS_PPU_SCC_H_B
		sta     FDS_PPU_SCC_V_B
		jmp     start_fdsstart
	; }
; ----------------------------
sub_EFDB:
	jsr     snd_FF5C

	ldx     #work_tbl_80
        lda     #work_tbl_80_end-1
        ldy     #work_tbl_A0_end-1
        jsr     fds_timers_tick
        rts
; -----------------------------------------------------------
tbl_EFE8:
	.byte	$FF, $FF
	.byte	$FF, $FF
	.byte	$FF, $FF
	.byte	$00, $00
	.byte	$FF, $FF

tbl_EFF2:
	.dbyt	$21A6
	.byte	$54
	.byte	" "
	.byte	NMI_LIST_END

tbl_EFF7:
	.dbyt	$21A6
	.byte	(:++)-(:+)
	: .byte	"PLEASE SET DISK CARD"
	: .byte	NMI_LIST_END

tbl_F00F:
	.dbyt	$21A6
	.byte	(:++)-(:+)
	: .byte	"NOW LOADING..."
	: .byte	NMI_LIST_END

tbl_F021:
	.byte	"DISK SET"
	.byte	"BATTERY "
	.byte	"A-B SIDE"
	.byte	"DISK NO."

tbl_F041:
	.dbyt	$21A6
	.byte	(:++)-(:+)
	: .byte	"DISK TROUBLE  ERR.20"
	: .byte	NMI_LIST_END

tbl_F059:
	.dbyt	$20E8
	.byte	(:++)-(:+)
	: .byte	"PRAM CRAM     OK"
	:

	.dbyt	$2168
	.byte	(:++)-(:+)
	: .byte	"PORT"
	:

tbl_F073:
	.dbyt	$3F00
	.byte	(:++)-(:+)
	: .byte	$0F, $20, $0F, $0F
	.byte	$0F, $0F, $0F, $0F
	:

	.dbyt	$2BC0
	.byte	$50
	.byte	"0"

	.dbyt	$2BD0
	.byte	$70
	.byte	$55

	.byte	NMI_LIST_END

tbl_F087:
	.byte   $80, $B8, $00, $00
	.byte	$00, $00, $00, $00
	.byte	$10, $00, $32, $00
	.byte	$00, $00, $01, $00
        .byte   $80, $B8, $00, $F0
	.byte	$00, $00, $00, $00
	.byte	$00, $01, $32, $18
	.byte	$00, $00, $FF, $00
; ----------------------------
sub_F0A7:
	lda     FDS_PPU_SCC_V_B
        beq     :+
        dec     FDS_PPU_SCC_V_B
        bne     :+ ; if (FDS_PPU_SCC_V_B && (--FDS_PPU_SCC_V_B == 0)) {
		lda     #$10
		sta     work_tbl_80+$14
	: ; }

	ldx     work_tbl_80+$14
        beq     :+ ; if (work_tbl_80[0x14]) {
		dex
		beq     sub_F0D4
		dex
		beq     sub_F0DA
		dex
		beq     sub_F0E0
	: ; }

	jsr     fds_set_obj_dma
        jsr     fds_ppu_inc_load
        lda     work_tbl_80+$12
        bne     :+ ; if (!work_tbl_80[0x14] && !work_tbl_80[0x12]) {
		jsr     fds_ppu_displist_write
		.addr	tbl_EFF2

		lda     #$40
		sta     work_tbl_80+$12
		rts
	sub_F0D4: ; } else if (work_tbl_80[0x14] == 1) {
		jsr     fds_ppu_displist_write
		.addr	tbl_F717
		rts
	sub_F0DA: ; } else if (work_tbl_80[0x14] == 2) {
		jsr     fds_ppu_displist_write
		.addr	tbl_F724
		rts
	sub_F0E0: ; } else if (work_tbl_80[0x14] == 3) {
		jsr     fds_ppu_displist_write
		.addr	tbl_F72D
		rts
	: cmp     #$2E
        bne     :+ ; } else if (!work_tbl_80[0x14] && work_tbl_80[0x12] == 0x2E) {
		jsr     fds_ppu_displist_write
		.addr	tbl_EFF7
	: ; }

	rts
; -----------------------------------------------------------
sub_F0F0:
	lda     #$F4
        ldx     #$02
	ldy	#$02
        jsr     fds_ram_fill

        ldy     #$20
	: ; for (y = 0x20; y > 0; y--) {
		lda	tbl_F087-1, y
		sta     work_tbl_C0-1, y

		dey
		bne     :-
	; }

        lda     #$D0
        sta     $60
        sta     $01
	LF10A:
		ldy     #$02
		ldx     #$60
		jsr     fds_rand

		lda     $60
		ldx     $01
		sta     $022F, x
		dex
		stx     $01
		bne     LF10A

        lda     #$18
        ldx     #$D0
	LF121:
		sta     $022D, x
		dex
		dex
		dex
		dex
		bne     LF121

        ldx     #$D0
        stx     $24
	LF12E:
		jsr     LF149
		cpx     #$D0
		bne     LF12E

        rts
; -----------------------------------------------------------
sub_F136:
	lda     work_tbl_80+$04
        bne     LF149
		lda     #$04
		sta     work_tbl_80+$04
		ldx     #$D0
		LF140:
			dec     $022C, x
			dex
			dex
			dex
			dex
			bne     LF140

	LF149:

	ldy     #$04
	LF14B:
		ldx     $24
		dec     $022E, x
		lda     #$03
		and     $022E, x
		ora     #$20
		sta     $022E, x
		dex
		dex
		dex
		dex
		bne     LF162
			ldx     #$D0
		LF162:
		stx     $24

		dey
		bne     LF14B

	rts
; -----------------------------------------------------------
tbl_F168:
	.byte	1, 2, 7, 8
; ----------------------------
sub_F16C:
	ldy     #$18
	: ; for (y = 0x18; y > 0; y--) {
		lda     tbl_F041-1, y
		sta     work_tbl_40-1, y

		dey
		bne     :-
	; }

        lda     $23
        and     #$0F
        sta     work_tbl_40+$16
        lda     $23
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        sta     work_tbl_40+$15
        cmp     #$02
        beq     LF1B0
		ldy     #$0E
		lda     #$24
		LF18D:
			sta     work_tbl_40+$02, y
			dey
			bne     LF18D

		ldy     #$05
		lda     $23
		LF197:
			dey
			beq     LF1B0
			cmp     tbl_F168-1, y
			bne     LF197

		tya
		asl     a
		asl     a
		asl     a
		tax
		ldy     #$07
		LF1A6:
			dex
			lda     tbl_F021, x
			sta     work_tbl_40+$03, y

			dey
			bpl     LF1A6
	LF1B0:

	jsr     fds_ppu_displist_write
	.addr	work_tbl_40
        rts
; -----------------------------------------------------------
sub_F1B6:
	lda     #$29
        sta     $0C
        lda     #$10
        sta     $08
        lda     #$00
        sta     $07
        lda     #$00
        sta     $09
        lda     #$E0
        sta     $0A
	LF1CA:
		ldy     #$00
		LF1CC:
			lda     ($09), y
			lsr     a
			sta     $0B
			iny
			ora     ($09), y
			eor     #$FF
			sta     $17, y
			lda     ($09), y
			eor     #$FF
			and     $0B
			eor     #$FF
			sta     $0F, y
			cpy     #$08
			bne     LF1CC

		lda     $07
		ldy     $08
		ldx     #$01
		jsr     fds_ppu_copy_to
		.addr	$10

		clc
		lda     #$10
		adc     $07
		sta     $07
		lda     #$00
		adc     $08
		sta     $08
		clc
		lda     #$08
		adc     $09
		sta     $09
		lda     #$00
		adc     $0A
		sta     $0A
		dec     $0C
		bne     LF1CA

        rts
; -----------------------------------------------------------
sub_F212:
	jsr     sub_F21F
        jsr     sub_F319
        jsr     sub_F2A0
        jsr     sub_F2F3
        rts
; -----------------------------------------------------------
sub_F21F:
	lda     $20, x
        bne     LF254
        lda     work_tbl_C0+$00, x
        bne     LF254
        lda     work_tbl_A0+$10
        bne     LF254
        lda     work_tbl_80+$01, x
        bne     LF254
		lda     $62, x
		and     #$3C
		sta     work_tbl_80+$01, x
		txa
        	bne     LF255
        lda     work_tbl_A0+$12
        bne     LF263
        lda     work_tbl_C0+$10
        beq     LF26A
        lda     $22
        bne     LF24A
        lda     work_tbl_C0+$03
        cmp     #$78
        bcc     LF27A
	LF24A:
		lda     #$00
		sta     work_tbl_C0+$08, x
		sta     work_tbl_C0+$0F, x
		lda     #$FF
		sta     work_tbl_C0+$0E, x
	LF254:
		rts

LF255:
	lda     work_tbl_C0+$00
        beq     LF287
        lda     $22
        bne     LF24A
        lda     $63, x
        cmp     #$80
        bcs     LF27A
	LF263:
		lda     #$00
		sta     work_tbl_C0+$0F, x
		sta     work_tbl_C0+$0E, x
		rts

LF26A:
	lda     work_tbl_C0+$08
        bne     LF274
        lda     $63, x
        cmp     #$C0
        bcc     LF24A
LF274:  lda     $64, x
        cmp     #$80
        bcc     LF263
	LF27A:
		lda     #$10
		sta     work_tbl_C0+$08, x
		lda     #$00
		sta     work_tbl_C0+$0F, x
		lda     #$01
		sta     work_tbl_C0+$0E, x
		rts

LF287:
	lda     $64, x
        ldy     work_tbl_C0+$08
        beq     LF291
        cmp     #$40
        bcc     LF27A
LF291:  cmp     #$C0
        bcc     LF263
		lda     #$40
		sta     work_tbl_C0+$0F, x
		lda     #$00
		sta     work_tbl_C0+$0E, x
		sta     work_tbl_C0+$08, x
		rts
; -----------------------------------------------------------
sub_F2A0:
	lda     $20, x
        beq     LF2D7
        bmi     LF2D8
		clc
		lda     #$30
		adc     work_tbl_C0+$0D, x
		sta     work_tbl_C0+$0D, x
		lda     #$00
		adc     work_tbl_C0+$0C, x
		sta     work_tbl_C0+$0C, x
		clc
		lda     work_tbl_C0+$0D, x
		adc     work_tbl_C0+$02, x
		sta     work_tbl_C0+$02, x
		lda     work_tbl_C0+$0C, x
		adc     work_tbl_C0+$01, x
		cmp     #$B8
		bcc     LF2D1
		txa
		bne     LF2E3
			lda     $60, x
			and     #$30
			sta     work_tbl_80+$01, x

		LF2CB:
			lda     #$00
			sta     $20, x
			lda     #$B8

		LF2D1:
			sta     work_tbl_C0+$01, x
			lda     #$03
			sta     work_tbl_C0+$05, x
	LF2D7:
		rts

	LF2D8:
		dec     $20, x
		lda     #$FD
		sta     work_tbl_C0+$0C, x
		lda     #$00
		sta     work_tbl_C0+$0D, x
		rts

	LF2E3:
		sta     work_tbl_C0+$08, x
		lda     #$01
		sta     work_tbl_C0+$0E, x
		lda     #$C0
		sta     work_tbl_C0+$0F, x
		lda     #$FF
		sta     work_tbl_80+$01, x
		bne     LF2CB
; -----------------------------------------------------------
sub_F2F3:
	lda     work_tbl_A0+$10
        bne     LF314
        lda     work_tbl_A0+$01, x
        bne     LF314
        lda     work_tbl_C0+$00, x
        beq     LF314 ; if () {
		lda     $62, x
		ora     #$10
		and     #$3C
		sta     work_tbl_80+$01, x
		ldy     #$10
		LF309: ; for () {
			lda     tbl_F087, x
			sta     work_tbl_C0+$00, x
			inx
			dey
			bne     LF309
		; }

		sty     work_tbl_A0+$10, x
	LF314: ; }

	rts
; -----------------------------------------------------------
LF315:
	.byte	0, 2, 1, 2
; ----------------------------
sub_F319:
	lda     work_tbl_C0, x
        bne     LF356
		clc
		lda     work_tbl_C0+$0F, x
		adc     work_tbl_C0+$04, x
		sta     work_tbl_C0+$04, x
		lda     work_tbl_C0+$0E, x
		adc     work_tbl_C0+$03, x
		ldy     work_tbl_A0+$10
		cpy     #$20
		bcs     LF342
		cmp     #$F8
		bcc     LF357
		cpy     #$1F
		bcs     LF342 ; if () {
			; if () {
				lda     $60, x
				and     #$2F
				ora     #$06
				sta     work_tbl_A0+$01, x
				lda     #$80
				sta     work_tbl_C0+$00, x
			LF342: ; }

			sta     work_tbl_C0+$03, x
			lsr     a
			lsr     a
			and     #$03
			tay
			lda     work_tbl_C0+$0E, x
			ora     work_tbl_C0+$0F, x
			bne     LF351 ; if () {
				ldy     #$01
			LF351: ; }

			lda     LF315, y
			sta     work_tbl_C0+$05, x
		; }
	LF356:
	rts

LF357:
	cmp     #$78
        bne     LF342
        cpx     $22
        bne     LF342
        ldy     $20, x
        bne     LF342 ; if () {
		ldy     #$00
		sty     work_tbl_C0+$0E, x
		sty     work_tbl_C0+$0F, x
		ldy     #$80
		sty     $20, x
		bne     LF342
	; }
; -----------------------------------------------------------
sub_F36F:
	lda     work_tbl_A0+$10
        bne     :+
        lda     work_tbl_C0+$00
        ora     work_tbl_C0+$10
        bne     :+
        clc
        lda     work_tbl_C0+$03
        adc     #$19
        cmp     work_tbl_C0+$13
        bcc     :+ ; if (!work_tbl_A0[0x10] && !(work_tbl_C0[0]|work_tbl_C0[0x10]) && ((work_tbl_C0[3] + 0x19) < work_tbl_C0[0x13])) {
		sta     work_tbl_C0+$13
		lda     #$02
		sta     work_tbl_C0+$0E
		sta     work_tbl_C0+$1E
		lda     #$00
		sta     work_tbl_C0+$0F
		sta     work_tbl_C0+$1F
		lda     #$10
		sta     work_tbl_C0+$08
		sta     work_tbl_C0+$18
		lda     #$30
		sta     work_tbl_A0+$10
	: ; }

	rts
; -----------------------------------------------------------
tbl_F39B:
	.byte	$2A, $0A
	.byte	$25, $05
	.byte	$21, $01
	.byte	$27, $16
; ----------------------------
sub_F3A3:
	ldy     #$08
        lda     work_tbl_80+$03
        bne     LF3F5 ; if () {
		lda     work_tbl_80+$13
		bne     LF41C
		ldx     #$00
		LF3AF: ; for () {
			lda     work_tbl_C0+$01, x
			cmp     #$A4
			bcs     LF3CB
			lda     #$20
			ldy     work_tbl_A0+$12
			bne     LF3C9
			lda     #$08
			ldy     $65
			cpy     #$18
			bcs     LF3C9 ; if () {
				lda     #$08
				sta     work_tbl_A0+$12
				lda     #$20
			LF3C9: ; } if () {
				sta     work_tbl_80+$03, x
			LF3CB: ; }

			cpx     #$10
			ldx     #$10
			bcc     LF3AF
		; }

		lda     $22
		beq     LF3F4
		lda     work_tbl_80+$02
		bne     LF3F4 ; if () {
			lda     #$08
			sta     work_tbl_80+$02

			ldx     #$0F
			lda     work_tbl_40+$07
			cmp     #$0F
			bne     :+ ; if (work_tbl_40[7] == 0x0F) {
				ldx     #$16
			: ; }
			stx     work_tbl_40+$07

		LF3E9:
			lda     #$3F
			ldx     #$08
			ldy     #$08
			jsr     fds_ppu_buffer_load
			.addr	work_tbl_40
		LF3F4: ; }

		rts
	LF3F5: ; } else

	lda     tbl_F636-1, y
        sta     work_tbl_40-1, y
        dey
        bne     LF3F5

        inc     $21
        lda     $21
        and     #$06
        tay
        lda     tbl_F39B, y
        sta     work_tbl_40+$02
        lda     tbl_F39B+1, y
        sta     work_tbl_40+$03
        ldy     #$00
        lda     work_tbl_A0+$12
        bne     LF417 ; if () {
        	ldy     #$10
	; }
LF417:
	sty     $22
	jmp	LF3E9

	LF41C: ; for () {
		lda     tbl_F641-1, y
		sta     work_tbl_40-1, y
		dey
		bne     LF41C
	; }
        beq     LF417
; -----------------------------------------------------------
sub_F427:
	jsr     fds_ppuctlr1_blk_on
        lda     #$00
        ldx     #$58
        ldy     #$13
        jsr     fds_ppu_copy_to
	.addr	tbl_F736
        lda     #$00
        ldx     #$19
        ldy     #$00
        jsr     fds_ppu_copy_to
	.addr	tbl_FCA6

        jsr     sub_F1B6

        lda     #$20
        ldx     #$6D
        ldy     #$AA
        jsr     fds_ppu_fill
        lda     #$28
        ldx     #$6D
        ldy     #$AA
        jsr     fds_ppu_fill
        jsr     fds_nmi_wait
        jsr     fds_ppu_displist_write
	.addr	tbl_F63E
	rts
; -----------------------------------------------------------
sub_F45E:
	jsr     fds_ppuctlr1_blk_on

        ldy     #$03
        jsr     sub_F4B9
        jsr     sub_F1B6

        jsr     fds_nmi_wait
        jsr     fds_ppu_displist_write
	.addr	tbl_F073
	lda	#$20
	ldx	#$24
        ldy     #$00
        jsr     fds_ppu_fill

        lda     FDS_PPU_CTLR0_B
        and     #<~(ppu_ctlr0::inc_32)
        sta     FDS_PPU_CTLR0_B
        sta     PPU_CTLR0
        ldx     PPU_SR
        ldx     #$28
        stx     PPU_VRAM_AR
        lda     #$00
        sta     PPU_VRAM_AR
        lda     PPU_VRAM_IO
        ldy     #$00
	LF495:
		lda     PPU_VRAM_IO
		cmp     security_code, y
		bne     LF4B0
		iny
		cpy     #$E0
		bne     LF495

		stx     PPU_VRAM_AR
		sty     PPU_VRAM_AR
		LF4A8:
			lda     #$24
			sta     PPU_VRAM_IO
			iny
			bne     LF4A8

	LF4B0:

	rts
; -----------------------------------------------------------
tbl_F4B1:
	.byte	$02, $30, $10, $29, $32, $00, $29, $10
; ----------------------------
sub_F4B9:
	ldx     #$03
	LF4BB:
		lda     tbl_F4B1, y
		sta     $07, x
		dey
		dex
		bpl     LF4BB

	lda     #$29
        sta     $0B
	LF4C8:
		lda     $07
		ldx     #$01
		ldy     $09
		jsr	fds_ppu_copy_to
		.addr	$10
		lda     $08
		ldx     #$01
		ldy     $0A
		jsr     fds_ppu_copy_to
		.addr	$10

		ldy     #$01
		LF4E0:  clc
			lda     #$10
			adc	a:$07, y
			sta     a:$07, y
			lda     #$00
			adc     a:$09, y
			sta     a:$09, y

			dey
			bpl     LF4E0

		dec     $0B
		bne     LF4C8

        rts
; -----------------------------------------------------------
sub_F4F9:
	lda     #$20
        ldx     #$24
        ldy     #$00
        jsr     fds_ppu_fill
        jsr     fds_nmi_wait
        jsr     fds_ppu_displist_write
	.addr	tbl_F059

	jsr	sub_F5FC
	bne	LF550
		lda	#$00
		ldx	#$00
		ldy	#$00
		jsr     fds_ppu_copy_to
		.addr	$C000

		lda	#$00
		ldx     #$00
		ldy     #$10
		jsr     fds_ppu_copy_to
		.addr	$D000

		lda	#$02
		ldx     #$00
		ldy     #$00
		jsr     fds_ppu_copy_to
		.addr	$C000

		lda	#$02
		ldx     #$00
		ldy     #$10
		jsr     fds_ppu_copy_to
		.addr	$D000

	lda	#$C0
	sta	work_00+1
	ldy	#$00
	sty	work_00
	ldx	#$20
        lda     #$7F
        adc     #$02
        jsr     sub_F61C
        beq     LF57B
		lda     work_00+1
		and     #$03
		sta     work_00+1

		LF550:
		lda     #$11
		sta     $0B

		ldy     #$03
		lda     work_00
		LF55C:
			tax
			and     #$0F
			sta     $07, y
			dey
			txa
			lsr     a
			lsr     a
			lsr     a
			lsr     a
			sta     $07, y
			lda     work_00+1

			dey
			bpl     LF55C
		
		lda     #$20
		ldx     #$F4
		ldy     #$05
		jsr     fds_ppu_buffer_load
		.addr	$07
	LF57B:

	jsr     sub_F1B6

        jsr     fds_joypad_stat

        lda     FDS_JOYPAD_HOLD_1
        cmp     #(joypad_button::face_a|joypad_button::right)
        bne     sub_F5B9 ; if (FDS_JOYPAD_HOLD_1_a|FDS_JOYPAD_HOLD_1_right) {
		jsr     fds_ppu_displist_write
		.addr	tbl_F598

		jsr	fds_nmi_wait

		jsr	fds_ppu_restore
		jsr     fds_ppuctlr1_bgblk_off

		: ; for (;;) {
			jmp     :-
		; }
	; }
; -----------------------------------------------------------
tbl_F598:
	.dbyt	$20E8
	.byte	$50
	.byte	" "

	.dbyt	$2163
	.byte	(:++)-(:+)
	: .byte	"PROGRAMED BY TAKAO SAWANO"
	: .byte	NMI_LIST_END
; -----------------------------------------------------------
sub_F5B9:
	lda     #$01
        sta     $0F
        lda     #$FF
	clc

        pha
        php
	: ; for (;;) {
		jsr     fds_nmi_wait

		jsr     fds_ppu_inc_load
		jsr     fds_ppu_restore
		jsr     fds_ppuctlr1_bgblk_off

		dec     $0F
		bne     :+ ; if () {
			plp
			pla

			sta     FDS_DISK_ACK
			rol     a
			pha
			php
			lda     #$19
			sta     $0F
		: ; }

		lda     FDS_EXT_READ
		ldx     #8-1
		: ; for (x = 8-1; x >= 0; x--) {
			ldy     #1
			asl     a
			bcs     :+ ; if (!(FDS_EXT_READ & 0x80)) {
				dey
			: ; }
			sty     $07, x

			dex
			bpl     :--
		; }
	
		lda     #$21
		ldx     #$70
		ldy     #$08
		jsr     fds_ppu_buffer_load
		.addr	$07

		jmp     :----
	; }
; -----------------------------------------------------------
sub_F5FC:
	lda     #$60
        ldx     #$80
        stx     work_03
        pha

        sta     work_00+1
        ldy     #0
        sty     work_00
        clv
        jsr     sub_F61C

        pla
        sta     work_00+1
        sty     work_00
        ldx     work_03
        lda     #$7F
        adc     #2
        jsr     sub_F61C
        rts
; -----------------------------------------------------------
sub_F61C:
	: ; for (x; x > 0; x--) {
		stx     work_02
		: ; for (y; y > 0 && (work[2] & 0x40); y--) {
			lda     work_02
			bvs     :++

			sta     (work_00), y
		:
			inc     work_02

			dey
			bne     :--
		; }

		inc     work_00+1
		dex
        	bne     :---
	; }
		
	; if () {
        	rts
	: cmp     (work_00), y
        beq     :-- ; } else {
		sty     work_00
		rts
	; }
