.include	"system/ppu.i"
.include	"system/ctrl.i"
.include	"system/fds.i"

.include	"tunables.i"
.include	"mem.i"

; -----------------------------------------------------------
	.export nmi
nmi:
        bit     FDS_STACK_NMIMODE
        bpl     :++
        bvc     :+ ; if (FDS_STACK_NMIMODE == game_vec_2) {
		jmp     (FDSINT2)
	: ; } else if (FDS_STACK_NMIMODE == game_vec_1) {
		jmp     (FDSINT1)
	: bvc     :+ ; } else if (FDS_STACK_NMIMODE == game_vec_0) {
		jmp     (FDSINT0)
	: ; } else { 
		lda     FDS_PPU_CTLR0_B
		and     #<~(ppu_ctlr0::int)
		sta     FDS_PPU_CTLR0_B
		sta     PPU_CTLR0

		lda     PPU_SR

		pla
		pla
		pla

		pla
		sta     FDS_STACK_NMIMODE

		pla
		rts
	; }
; -----------------------------------------------------------
	.export fds_nmi_wait
fds_nmi_wait:
	pha
        lda     FDS_STACK_NMIMODE
        pha

        lda     #fds_nmimode::none
        sta     FDS_STACK_NMIMODE

        lda     FDS_PPU_CTLR0_B
        ora     #ppu_ctlr0::int
        sta     FDS_PPU_CTLR0_B
        sta     PPU_CTLR0
	: ; for (;;) {
		bne     :-
	; }
; -----------------------------------------------------------
	.export irq
irq:
        bit     FDS_STACK_IRQMODE
        bmi     :+++
        bvc     :+ ; if (FDS_STACK_IRQMODE == transfer) {
		ldx     FDS_DATA_READ
		sta     FDS_DATA_WRITE

		pla
		pla
		pla
		txa
		rts
	: ; } else if (FDS_STACK_IRQMODE == skip_bytes) {
		pha

		lda     FDS_STACK_IRQMODE
		sec
		sbc     #1
		bcc     :+ ; if (FDS_STACK_IRQMODE >= 1) {
			sta     FDS_STACK_IRQMODE
			lda     FDS_DATA_READ
		: ; }

		pla
		rti
	: bvc	:+ ; } else if (FDS_STACK_IRQMODE == game_vec) {
        	jmp     (FDSIRQ)
	: ; } else {
		pha

		lda     FDS_MEDIA_SR
		jsr     fds_sleep_short

		pla
		rti
	; }
