.include	"tunables.i"

	.export tbl_F636
tbl_F636:
	.byte	$0F, $30
	.byte	$27, $16
	.byte	$0F, $10
	.byte	$00, $16

	.export tbl_F63E
tbl_F63E:
	.byte	$3F, $08, $18

	.export tbl_F641
tbl_F641:
	.incbin "data/tbl_F641.bin"

	.export tbl_F717
tbl_F717:
	.dbyt	$2040
	.byte	$60
	.byte	$80

	.dbyt	$2020
	.byte	$60
	.byte	$81

	.dbyt	$2000
	.byte	$60
	.byte	$81

	.byte	NMI_LIST_END

	.export tbl_F724
tbl_F724:
	.dbyt	$2340
	.byte	$60
	.byte	$80

	.dbyt	$2360
	.byte	$60
	.byte	$81

	.byte	NMI_LIST_END

	.export tbl_F72D
tbl_F72D:
	.dbyt	$2380
	.byte	$60
	.byte	$82

	.dbyt	$23A0
	.byte	$60
	.byte	$82

	.byte	NMI_LIST_END

	.export tbl_F736
tbl_F736:
	.incbin "data/tbl_F736.bin"

	.export tbl_FCA6
tbl_FCA6:
	.incbin "data/tbl_FCA6.bin"
