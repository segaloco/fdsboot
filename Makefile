#
# Famicom Disk System Boot ROM
#
AS	=	ca65
ASFLAGS	=	-DCONS -U -I ./inc
LD	=	ld65
LDFLAGS	=	-C link.ld
CHECK	=	tools/check.sh

CKSUM	=	882518837

BIN	=	fdsbios.bin

OBJS	=	data/font.o \
		src/lib/sleep.o \
		src/lib/blk.o \
		src/int.o \
		src/lib/disk.o \
		src/lib/ppu.o \
		src/lib/ppu_px_nt.o \
		src/lib/rand.o \
		src/lib/obj_dma.o \
		src/lib/timers.o \
		src/lib/joypad.o \
		src/lib/ppu_fill.o \
		src/lib/ram_fill.o \
		src/lib/ppu_restore.o \
		src/lib/tbljmp.o \
		src/lib/ctrl.o \
		src/lib/ppu_copyto.o \
		src/main.o \
		src/frdt.o \
		src/snd.o

all: $(BIN)

check: $(BIN)
	$(CHECK) $(BIN) $(CKSUM)

$(BIN): $(OBJS)
	$(LD) $(OBJS) $(LDFLAGS)

.s.o:
	$(AS) $(ASFLAGS) -o $@ $<

clean:
	rm -rf $(BIN) $(OBJS)
